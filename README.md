# Banque service

Ce service permet d'effecuer des opérations bancaire en back_end
    


## Bâtir le projet
Dans une console, lancer la commande :
```
gradlew clean build
```
## Démarrage des applications
À partir de la racine du dépot Git , lancez la commande suivante pour démarrer l'api banque en back-end

```
./autoStartBanque.sh
```
Si le démarrage est un succés 
![Désolé un probelem de chargement d'image ...](/document/images/autoStart.PNG)
# Architecture

![Désolé un probelem de chargement d'image ...](/document/images/BanqueAPIDiagram.png)