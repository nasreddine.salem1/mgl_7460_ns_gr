#!/bin/bash

if [ -L $0 ] ; then
	    _dir=$(dirname $(readlink -f $0)) 
    else
	    _dir=$(dirname $0) 
fi
# Clear screen
clear
# Batire les exécutable
echo "########################################################################"
echo "        Démarrage de la compiltaion et creéation des micorservices :    "
echo "1- BanqueApi                                                            "
echo "2- ClientBanque                                                         "
echo "3- EmployeeBanque                                                       "
echo "########################################################################"
./gradlew clean build
clear
# Démarrage de l'application banque Api en back-end
echo "Lancement de banqueApi en arriere plan"
nohup java -jar $_dir/banqueApi/build/libs/banqueApi.jar &
# countdown to start app
echo "Démarrage sera dans  : "
for i in {20..01}
do
    tput cup 1 23
    echo -n "$i"
    sleep 1
done
clear
echo ""
echo "#########################################################################"
echo "### Statut de l'api banque en arriere plan                            ###"
echo "#########################################################################"
echo "Statut : "
curl http://localhost:8787/actuator/health
echo ""
echo "Api information : "
curl http://localhost:8787/actuator/info
echo ""





 