package com.uqam.banqueApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BanqueApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BanqueApiApplication.class, args);
	}

}
