package com.uqam.banqueApi.controleur;

import com.uqam.api.rest.v1.controleur.ClientApi;
import com.uqam.api.rest.v1.ressource.AllAvailProduitRessource;
import com.uqam.api.rest.v1.ressource.AllProduitRessource;
import com.uqam.banqueApi.domaine.entite.Product;
import com.uqam.banqueApi.domaine.entite.ProductEnum;
import com.uqam.banqueApi.domaine.service.ProductService;
import com.uqam.banqueApi.mapper.BanqueProductMapping;
import com.uqam.banqueApi.mapper.ProductMapping;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/")
@Validated
public class ClientControleur implements ClientApi {


    private final ProductMapping productMapping;
    private final BanqueProductMapping banqueProductMapping;
    private final ProductService productService;

    public ClientControleur(ProductMapping productMapping, BanqueProductMapping banqueProductMapping, ProductService productService) {
        this.productMapping = productMapping;
        this.banqueProductMapping = banqueProductMapping;
        this.productService = productService;
    }

    @Override
    public ResponseEntity<AllProduitRessource> listAllClientProduct(String clientName) {
        AllProduitRessource produitRessources;

        List<Product> products = new ArrayList<>(productService.findAllProduct(clientName));
        produitRessources = products.stream()
            .map(product -> productMapping.toRessource(product))
            .collect(Collectors.toCollection(AllProduitRessource::new));

        return new ResponseEntity<>(produitRessources, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<AllAvailProduitRessource> listAllClientProductAvail(String clientName) {
        AllAvailProduitRessource banqueProduitRessources;

        List<ProductEnum> productEnums = productService.findAllBanqueProduct(clientName);
        banqueProduitRessources = productEnums.stream()
            .map(productEnum -> banqueProductMapping.toResource(productEnum))
            .collect(Collectors.toCollection(AllAvailProduitRessource::new));

        return new ResponseEntity<>(banqueProduitRessources, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> suscribeProductForClient(String clientName, Integer produitId) {
        productService.suscribeProduit(clientName, produitId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> unsuscribeProductForClient(String clientName, Integer produitId) {

        productService.unsuscribeProduit(clientName, produitId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
