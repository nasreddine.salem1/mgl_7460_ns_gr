package com.uqam.banqueApi.controleur;

import com.uqam.api.rest.v1.controleur.EmployeeApi;
import com.uqam.api.rest.v1.ressource.AllClientRessource;
import com.uqam.api.rest.v1.ressource.AllProduitRessource;
import com.uqam.api.rest.v1.ressource.ClientRessource;
import com.uqam.api.rest.v1.ressource.ProduitRessource.StatutEnum;
import com.uqam.banqueApi.domaine.entite.Client;
import com.uqam.banqueApi.domaine.entite.Product;
import com.uqam.banqueApi.domaine.service.ClientService;
import com.uqam.banqueApi.domaine.service.ProductService;
import com.uqam.banqueApi.mapper.ClientMapping;
import com.uqam.banqueApi.mapper.ProductMapping;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/")
@Validated
public class EmployeeControleur implements EmployeeApi {

    private final ClientMapping clientMapping;
    private final ProductMapping productMapping;
    private final ClientService clientService;
    private final ProductService productService;

    public EmployeeControleur(ClientMapping clientMapping, ProductMapping productMapping, ClientService clientService, ProductService productService) {
        this.clientMapping = clientMapping;
        this.productMapping = productMapping;
        this.clientService = clientService;
        this.productService = productService;
    }


    @Override
    public ResponseEntity<Void> addProductForClient(String clientName, Integer produitId) {
        productService.addOrRejectProduct(clientName, produitId, StatutEnum.ACCEPT);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ClientRessource> createClient(@Valid ClientRessource body) {
        Client client = clientMapping.toDomaine(body);
        ClientRessource resultat = null;
        resultat = clientMapping.toRessource(clientService.addClient(client));
        return new ResponseEntity<>(resultat, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<AllClientRessource> listAllClientWithWaitProductStatus() {
        AllClientRessource clientRessources;
        List<Client> clients = clientService.findAllClientWithProductAttente();
        clientRessources = clients.stream()
            .map(client -> clientMapping.toRessource(client))
            .collect(Collectors.toCollection(AllClientRessource::new));

        return new ResponseEntity<>(clientRessources, HttpStatus.OK);

    }

    @Override
    @ResponseBody
    public ResponseEntity<AllProduitRessource> listAllProductForClient(String clientName) {
        AllProduitRessource produitRessources;

        List<Product> products = new ArrayList<>(productService.findAllProduct(clientName));
        produitRessources = products.stream()
            .map(product -> productMapping.toRessource(product))
            .collect(Collectors.toCollection(AllProduitRessource::new));

        return new ResponseEntity<>(produitRessources, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> rejectProductForClient(String clientName, Integer produitId) {
        productService.addOrRejectProduct(clientName, produitId, StatutEnum.REJECT);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> updateStatusClient(@Valid ClientRessource body) {
        Client client = clientMapping.toDomaine(body);
        clientService.updateStatut(client);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ResponseBody
    @ExceptionHandler(HttpMediaTypeNotAcceptableException.class)
    public String handleHttpMediaTypeNotAcceptableException() {
        return "acceptable MIME type:" + MediaType.APPLICATION_JSON_VALUE;
    }
}
