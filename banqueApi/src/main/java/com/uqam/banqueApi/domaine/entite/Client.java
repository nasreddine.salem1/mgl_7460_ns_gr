package com.uqam.banqueApi.domaine.entite;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "CLIENT")
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_client", updatable = false, nullable = false)
    private Long clientId;

    @Column(name = "nom_client", nullable = false)
    @NotBlank(message = "Le nom est obligatoire")
    @Size(min = 1, max = 20, message
        = "Le nom du client doit etre entre 1 et 20 caractere")
    private String clientName;

    @Column(name = "email_client")
    private String email;

    @Column(name = "numero_telephone_client")
    private String phone;

    @Column(name = "status_client", nullable = false)
    private Integer clientStatut;


    @OneToMany(mappedBy = "client")
    private Set<Product> items;

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getClientStatut() {
        return clientStatut;
    }

    public void setClientStatut(Integer clientStatut) {
        this.clientStatut = clientStatut;
    }

    public Set<Product> getItems() {
        return items;
    }

    public void setItems(Set<Product> items) {
        this.items = items;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Client)) {
            return false;
        }
        Client client = (Client) o;
        return clientName.equals(client.clientName) &&
               Objects.equals(email, client.email) &&
               Objects.equals(phone, client.phone) &&
               clientStatut.equals(client.clientStatut);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientName, email, phone, clientStatut);
    }
}
