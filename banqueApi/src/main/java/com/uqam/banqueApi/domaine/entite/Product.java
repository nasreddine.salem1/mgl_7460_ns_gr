package com.uqam.banqueApi.domaine.entite;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "PRODUCT")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_id", updatable = false, nullable = false)
    private Long productId;

    // @Enumerated(EnumType.ORDINAL)
    @Column(name = "product_name")
    private String productName;

    @Column(name = "product_status")
    private String productStatus;


    @ManyToOne
    @JoinColumn(name = "id_client", nullable = false)
    private Client client;

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public Product() {
    }

    public Product(Long id, String name, String productStatus, Client client) {
        this.client = client;
        this.productId = id;
        this.productName = name;
        this.productStatus = productStatus;

    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(String productStatus) {
        this.productStatus = productStatus;
    }
}
