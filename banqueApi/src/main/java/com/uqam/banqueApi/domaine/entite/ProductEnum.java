package com.uqam.banqueApi.domaine.entite;

public enum ProductEnum {
    CP("COMPTE", 1L),
    VI("VISA", 2L),
    MA("MARGE", 3L),
    FI("FINANCEMENT", 4L),
    SO("SANS-OBJET", 5L);

    public final String nomProduit;
    public final Long idProduit;

    private ProductEnum(String produit, Long id) {
        this.nomProduit = produit;
        this.idProduit = id;
    }
}
