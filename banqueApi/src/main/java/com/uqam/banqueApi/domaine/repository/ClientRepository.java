package com.uqam.banqueApi.domaine.repository;

import com.uqam.banqueApi.domaine.entite.Client;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

    List<Client> findByClientNameContaining(String clientName);

    @Transactional
    @Modifying
    @Query("update Client set clientStatut= :statut where clientId = :clientId")
    void updateStatut(Long clientId, int statut);


    @Query("select c from Client c, Product p  where c.clientId=p.client.clientId and p.productStatus like 'ATTENTE'")
    List<Client> findAllClientForProductStatusAttente();
}
