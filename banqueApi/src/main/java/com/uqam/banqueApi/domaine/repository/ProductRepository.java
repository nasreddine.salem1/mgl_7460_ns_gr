package com.uqam.banqueApi.domaine.repository;


import com.uqam.banqueApi.domaine.entite.Client;
import com.uqam.banqueApi.domaine.entite.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    Set<Product> findByClient(Client client);

    Product findByClientAndProductName(Client client, String productName);
}
