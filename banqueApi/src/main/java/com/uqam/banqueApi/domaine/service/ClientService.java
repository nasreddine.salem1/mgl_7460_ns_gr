package com.uqam.banqueApi.domaine.service;

import com.uqam.banqueApi.domaine.entite.Client;

import java.util.List;


public interface ClientService {

    Client addClient(Client client);

    List<Client> findClient(Client clientdemander);

    void updateStatut(Client clientdemander);

    List<Client> findAllClientWithProductAttente();


}
