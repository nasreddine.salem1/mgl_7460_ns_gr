package com.uqam.banqueApi.domaine.service;

import com.uqam.banqueApi.domaine.entite.Client;

import com.uqam.banqueApi.domaine.repository.ClientRepository;
import com.uqam.banqueApi.exception.ValidationException;
import org.springframework.stereotype.Service;


import java.util.Collections;
import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {

    public static final String MSG_CLIENT_STATUT_NOT_NULL = "msg.client.statut.notNull";
    private ClientRepository clientRepository;

    public ClientServiceImpl(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Override
    public Client addClient(Client client) {
        if (client.getClientName() != null) {
            if (client.getClientStatut() != null) {
                if (clientRepository.findByClientNameContaining(client.getClientName().toLowerCase()).size() == 0) {
                    client.setClientName(client.getClientName().toLowerCase());
                    return clientRepository.save(client);
                } else {
                    throw new ValidationException("msg.client.nom.existe");
                }
            } else {
                throw new ValidationException(MSG_CLIENT_STATUT_NOT_NULL);
            }
        } else {
            throw new ValidationException("msg.client.name.notNull");
        }
    }

    @Override
    public List<Client> findClient(Client clientdemander) {

        if (clientdemander.getClientName() != null) {
            if (clientdemander.getClientStatut() != null) {
                return clientRepository.findByClientNameContaining(clientdemander.getClientName().toLowerCase());
            } else {
                throw new ValidationException(MSG_CLIENT_STATUT_NOT_NULL);
            }
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    public void updateStatut(Client clientdemander) {

        if (clientdemander.getClientName() != null) {
            if (clientdemander.getClientStatut() != null) {
                clientRepository.updateStatut(clientdemander.getClientId(), clientdemander.getClientStatut());
            } else {
                throw new ValidationException(MSG_CLIENT_STATUT_NOT_NULL);
            }
        }
    }

    @Override
    public List<Client> findAllClientWithProductAttente() {
        return clientRepository.findAllClientForProductStatusAttente();
    }
}
