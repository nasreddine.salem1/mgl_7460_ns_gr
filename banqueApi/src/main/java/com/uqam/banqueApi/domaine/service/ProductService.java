package com.uqam.banqueApi.domaine.service;

import com.uqam.api.rest.v1.ressource.ProduitRessource.StatutEnum;
import com.uqam.banqueApi.domaine.entite.Product;
import com.uqam.banqueApi.domaine.entite.ProductEnum;

import java.util.List;
import java.util.Set;


public interface ProductService {
    Set<Product> findAllProduct(String clientName);

    List<ProductEnum> findAllBanqueProduct(String clientName);

    Product suscribeProduit(String clientName, Integer produitId);

    Product unsuscribeProduit(String clientName, Integer produitId);

    Product findAllProductForClientName(String clientName, Integer produitId);

    void addOrRejectProduct(String clientName, Integer produitId, StatutEnum statut);

}
