package com.uqam.banqueApi.domaine.service;

import com.uqam.api.rest.v1.ressource.ProduitRessource.StatutEnum;
import com.uqam.banqueApi.domaine.entite.Client;
import com.uqam.banqueApi.domaine.entite.Product;
import com.uqam.banqueApi.domaine.entite.ProductEnum;
import com.uqam.banqueApi.domaine.repository.ClientRepository;
import com.uqam.banqueApi.domaine.repository.ProductRepository;
import com.uqam.banqueApi.exception.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import javax.crypto.spec.OAEPParameterSpec;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepositor;
    @Autowired
    private ClientRepository clientRepository;

    @Override
    public Set<Product> findAllProduct(String clientName) {
        if (clientName != null) {
            List<Client> client = clientRepository.findByClientNameContaining(clientName.toLowerCase());
            if (client.size() == 1) {
                return productRepositor.findByClient(client.get(0));
            }
            if (client.size() == 0) {
                return Collections.emptySet();
            } else {
                throw new ValidationException("msg.client.name.multiple");
            }
        } else {
            throw new ValidationException("msg.client.name.notNull");
        }
    }

    @Override
    public List<ProductEnum> findAllBanqueProduct(String clientName) {
        if (clientName != null) {
            List<Client> client = clientRepository.findByClientNameContaining(clientName);
            if (client.size() == 1) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(ProductEnum.CP);
                arrayList.add(ProductEnum.FI);
                arrayList.add(ProductEnum.MA);
                arrayList.add(ProductEnum.VI);
                return arrayList;
            }
            if (client.size() == 0) {
                return Collections.emptyList();
            } else {
                throw new ValidationException("msg.client.name.multiple");
            }
        } else {
            throw new ValidationException("msg.client.name.notNull");
        }
    }

    @Override
    public Product suscribeProduit(String clientName, Integer produitId) {
        Product product = new Product();
        if (clientName != null) {
            Client client = clientRepository.findByClientNameContaining(clientName.toLowerCase()).get(0);
            Optional<Product> productTrouver = Optional.ofNullable(productRepositor.findByClientAndProductName(client, findProductName(produitId).nomProduit));
            if ((!productTrouver.isPresent() || productTrouver.get().getProductStatus() != "ATTENTE")) {
                if (produitId == ProductEnum.VI.idProduit.intValue()) {
                    product.setProductStatus(StatutEnum.ACCEPT.name());
                } else {
                    product.setProductStatus(StatutEnum.ATTENTE.name());
                }
                product.setProductName(findProductName(produitId).nomProduit);
                product.setClient(client);
                productRepositor.save(product);
                return product;
            } else {
                throw new ValidationException("msg.product.id.notNullOrExist");
            }
        } else {
            throw new ValidationException("msg.client.name.notNull");
        }
    }

    @Override
    public Product unsuscribeProduit(String clientName, Integer produitId) {
        if (clientName != null) {
            Client client = clientRepository.findByClientNameContaining(clientName.toLowerCase()).get(0);
            if (produitId != null) {
                Product product = productRepositor.findByClientAndProductName(client, findProductName(produitId).nomProduit);
                product.setProductStatus(StatutEnum.TODELETE.name());
                productRepositor.save(product);
                return product;
            } else {
                throw new ValidationException("msg.client.name.notNull");
            }
        } else {
            throw new ValidationException("msg.product.id.notNull");
        }
    }

    @Override
    public Product findAllProductForClientName(String clientName, Integer produitId) {
        if (clientName != null) {
            Client client = clientRepository.findByClientNameContaining(clientName.toLowerCase()).get(0);
            if (produitId != null) {
                return productRepositor.findByClientAndProductName(client, findProductName(produitId).nomProduit);
            } else {
                throw new ValidationException("msg.client.name.notNull");
            }
        } else {
            throw new ValidationException("msg.product.id.notNull");
        }
    }

    @Override
    public void addOrRejectProduct(String clientName, Integer produitId, StatutEnum statut) {
        Product products = findAllProductForClientName(clientName, produitId);
        if (products != null && products.getProductStatus().equals("ATTENTE")) {
            products.setProductStatus(statut.name());
            productRepositor.save(products);
        } else {
            throw new ValidationException("msg.product.id.notNull");
        }
    }


    private ProductEnum findProductName(Integer produitId) {
        switch (produitId) {
            case 1:
                return ProductEnum.CP;
            case 2:
                return ProductEnum.VI;
            case 3:
                return ProductEnum.MA;
            case 4:
                return ProductEnum.FI;
            default:
                return ProductEnum.SO;
        }

    }
}
