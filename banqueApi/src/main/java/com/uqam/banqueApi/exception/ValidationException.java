package com.uqam.banqueApi.exception;

import java.io.Serializable;
import java.util.Arrays;

public class ValidationException extends RuntimeException {

    private final String msg;
    private final Serializable[] parametres;

    public ValidationException(final String msg, final Serializable... args) {
        this.msg = msg;
        this.parametres = args;
    }

    public String getMsg() {
        return msg;
    }

    public Serializable[] getParametres() {
        return parametres;
    }

    @Override
    public String getMessage() {
        return String.format("message=%s, parametres=%s", msg, Arrays.toString(parametres));
    }
}
