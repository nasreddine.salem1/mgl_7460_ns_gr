package com.uqam.banqueApi.mapper;


import com.uqam.api.rest.v1.ressource.BanqueProduitRessource;
import com.uqam.banqueApi.domaine.entite.ProductEnum;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;


@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface BanqueProductMapping {

    default BanqueProduitRessource toResource(ProductEnum productEnum) {
        if (productEnum == null) {
            return null;
        }
        BanqueProduitRessource banqueProduitRessource = new BanqueProduitRessource();
        banqueProduitRessource.setId(productEnum.idProduit);
        banqueProduitRessource.setProduitDescrition(productEnum.nomProduit);
        return banqueProduitRessource;
    }
}
