package com.uqam.banqueApi.mapper;

import com.uqam.api.rest.v1.ressource.ClientRessource;
import com.uqam.banqueApi.domaine.entite.Client;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy= ReportingPolicy.IGNORE)
public interface ClientMapping {

    Client toDomaine(ClientRessource clientRessource);

    ClientRessource toRessource(Client client);




}
