package com.uqam.banqueApi.mapper;

import com.uqam.api.rest.v1.ressource.ProduitRessource;
import com.uqam.banqueApi.domaine.entite.Product;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ProductMapping {

    @Mapping(source = "productId", target = "id")
    @Mapping(source = "productName", target = "nomProduit")
    @Mapping(source = "productStatus", target = "statut")
    @Mapping(source = "client.clientId", target = "clientId")
    ProduitRessource toRessource(Product product);

    Product toDomaine(ProduitRessource body);
}
