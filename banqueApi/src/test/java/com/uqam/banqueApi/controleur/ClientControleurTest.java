package com.uqam.banqueApi.controleur;

import com.uqam.api.rest.v1.ressource.BanqueProduitRessource;
import com.uqam.api.rest.v1.ressource.ProduitRessource;
import com.uqam.api.rest.v1.ressource.ProduitRessource.StatutEnum;
import com.uqam.banqueApi.domaine.entite.Client;
import com.uqam.banqueApi.domaine.entite.Product;
import com.uqam.banqueApi.domaine.entite.ProductEnum;
import com.uqam.banqueApi.domaine.service.ProductService;
import com.uqam.banqueApi.mapper.BanqueProductMapping;
import com.uqam.banqueApi.mapper.ProductMapping;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ClientControleurTest {

    @Mock
    ProductMapping productMapping;

    @Mock
    ProductService productService;
    @Mock
    BanqueProductMapping banqueProductMapping;

    @InjectMocks
    ClientControleur clientControleur;

    private ProduitRessource produitRessource;
    private BanqueProduitRessource banqueProduitRessource;

    @Before
    public void init() {

        produitRessource = new ProduitRessource();
        produitRessource.setNomProduit(ProductEnum.CP.nomProduit);
        produitRessource.setStatut(StatutEnum.ATTENTE);
        produitRessource.setClientId(1L);
        banqueProduitRessource = new BanqueProduitRessource();
        banqueProduitRessource.setProduitDescrition("COMPTE");
        banqueProduitRessource.setId(2L);

    }

    @Test
    public void testAffichgeProduitPourUnClient() {
        Client client = new Client();
        client.setClientStatut(1);
        client.setClientName("Salem");
        Product product = new Product(1L, ProductEnum.CP.nomProduit, "ATTENTE", client);
        Set<Product> products = new HashSet<>();
        products.add(product);
        Mockito.when(productService.findAllProduct(Mockito.anyString())).thenReturn(products);
        Mockito.when(productMapping.toRessource(product)).thenReturn(produitRessource);
        clientControleur.listAllClientProduct("Salem");
        verify(productMapping, times(1)).toRessource(product);
        verify(productService, times(1)).findAllProduct("Salem");
    }

    @Test
    public void testerAffichageDesProduitDisponible() {
        List<ProductEnum> productEnums = new ArrayList<>();

        productEnums.add(ProductEnum.FI);
        Mockito.when(productService.findAllBanqueProduct(Mockito.anyString())).thenReturn(productEnums);
        clientControleur.listAllClientProductAvail("toto");
        verify(banqueProductMapping, times(1)).toResource(productEnums.get(0));
    }

    @Test
    public void testerLaSbsciptionDunProduit() {
        clientControleur.suscribeProductForClient("toto", 1);
        verify(productService, times(1)).suscribeProduit("toto", 1);
    }

    @Test
    public void testerLaUnsbscribeduProduit() {
        clientControleur.unsuscribeProductForClient("toto", 1);
        verify(productService, times(1)).unsuscribeProduit("toto", 1);
    }

}