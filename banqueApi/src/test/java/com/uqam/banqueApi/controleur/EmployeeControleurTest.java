package com.uqam.banqueApi.controleur;

import com.uqam.api.rest.v1.ressource.ClientRessource;
import com.uqam.api.rest.v1.ressource.ProduitRessource;
import com.uqam.api.rest.v1.ressource.ProduitRessource.StatutEnum;
import com.uqam.banqueApi.domaine.entite.Client;
import com.uqam.banqueApi.domaine.entite.Product;
import com.uqam.banqueApi.domaine.entite.ProductEnum;
import com.uqam.banqueApi.domaine.service.ClientService;
import com.uqam.banqueApi.domaine.service.ProductService;
import com.uqam.banqueApi.mapper.ClientMapping;
import com.uqam.banqueApi.mapper.ProductMapping;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeControleurTest {

    @Mock
    ClientMapping clientMapping;

    @Mock
    ProductMapping productMapping;

    @Mock
    ClientService clientService;

    @Mock
    ProductService productService;


    @InjectMocks
    EmployeeControleur employeeControleur;

    private ClientRessource clientRessource;
    private ProduitRessource produitRessource;

    @Before
    public void init() {
        clientRessource = new ClientRessource();
        clientRessource.setClientName("test");
        clientRessource.setEmail("test@gamil.com");
        clientRessource.setPhone("5551116666");
        clientRessource.setClientStatut(1);

        produitRessource = new ProduitRessource();
        produitRessource.setNomProduit(ProductEnum.CP.nomProduit);
        produitRessource.setStatut(StatutEnum.ATTENTE);
        produitRessource.setClientId(1L);
    }


    @Test
    public void testCreationClientAvecSucces() {
        Client client = new Client();

        Mockito.when(clientMapping.toDomaine(clientRessource)).thenReturn(client);
        Mockito.when(clientService.addClient(client)).thenReturn(client);
        employeeControleur.createClient(clientRessource);

        verify(clientMapping, times(1)).toDomaine(clientRessource);
        verify(clientService, times(1)).addClient(client);


    }

    @Test
    public void testMiseAjourStatut() {
        clientRessource.setClientStatut(5);
        Client client = new Client();

        Mockito.when(clientMapping.toDomaine(clientRessource)).thenReturn(client);
        Mockito.doNothing().when(clientService).updateStatut(client);

        employeeControleur.updateStatusClient(clientRessource);
        verify(clientMapping, times(1)).toDomaine(clientRessource);
        verify(clientService, times(1)).updateStatut(client);

    }

    @Test
    public void testListerProduitPourUnClient() {
        Client client = new Client();
        client.setClientStatut(1);
        client.setClientName("Salem");
        Product product = new Product(1L, ProductEnum.CP.nomProduit, StatutEnum.ATTENTE.name(), client);
        Set<Product> products = new HashSet<>();
        products.add(product);
        Mockito.when(productMapping.toRessource(product)).thenReturn(produitRessource);
        Mockito.when(productService.findAllProduct(Mockito.anyString())).thenReturn(products);
        employeeControleur.listAllProductForClient("Salem");
        verify(productMapping, times(1)).toRessource(product);
        verify(productService, times(1)).findAllProduct("Salem");
    }

    @Test
    public void testerLeRejetDunProduit() {
        employeeControleur.rejectProductForClient("toto", 1);
        verify(productService, times(1)).addOrRejectProduct("toto", 1, StatutEnum.REJECT);
    }

    @Test
    public void testerlajoutDuProduit() {
        employeeControleur.addProductForClient("toto", 1);
        verify(productService, times(1)).addOrRejectProduct("toto", 1, StatutEnum.ACCEPT);
    }

    @Test
    public void testerlistAllClientWithWaitProductStatus() {
        Client client = new Client();
        client.setClientStatut(1);
        client.setClientName("Salem");
        List<Client> clients = new ArrayList<>();
        clients.add(client);
        Mockito.when(clientService.findAllClientWithProductAttente()).thenReturn(clients);
        employeeControleur.listAllClientWithWaitProductStatus();
        verify(clientMapping, times(1)).toRessource(clients.get(0));
    }
}