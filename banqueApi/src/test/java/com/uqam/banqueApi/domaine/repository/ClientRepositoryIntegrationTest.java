package com.uqam.banqueApi.domaine.repository;


import com.uqam.api.rest.v1.ressource.ProduitRessource.StatutEnum;
import com.uqam.banqueApi.domaine.entite.Client;
import com.uqam.banqueApi.domaine.entite.Product;
import com.uqam.banqueApi.domaine.entite.ProductEnum;
import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static junit.framework.TestCase.assertTrue;

@RunWith(SpringRunner.class)
@DataJpaTest
@DirtiesContext
public class ClientRepositoryIntegrationTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private ClientRepository clientRepository;


    @Test
    public void whenSauvgarderUnClientAlorsRetourneClient() {
        // given
        Client client = new Client();
        client.setClientName("toto");
        client.setEmail("toto@gmail.com");
        client.setClientStatut(1);
        testEntityManager.persistAndFlush(client);

        // when
        Optional<Client> trouver = clientRepository.findById(client.getClientId());
        // then
        assertTrue(trouver.isPresent());
        Assert.assertEquals(trouver.get().getClientId(), client.getClientId());

    }

    @Test
    public void testTrouverLesClientAvecProduitEnAttente() {
        // given
        Client client = new Client();
        client.setClientName("toto");
        client.setEmail("toto@gmail.com");
        client.setClientStatut(1);
        testEntityManager.persistAndFlush(client);

        Product product = new Product();
        product.setClient(client);
        product.setProductName(ProductEnum.CP.nomProduit);
        product.setProductStatus(StatutEnum.ATTENTE.name());

        Product product1 = new Product();
        product1.setClient(client);
        product1.setProductName(ProductEnum.FI.nomProduit);
        product1.setProductStatus(StatutEnum.ACCEPT.name());

        testEntityManager.persistAndFlush(client);
        testEntityManager.persistAndFlush(product);
        testEntityManager.persistAndFlush(product1);

        List<Client> clients = clientRepository.findAllClientForProductStatusAttente();

        Assert.assertEquals(1, clients.size());

    }


    @Test
    public void whenRecherhceUnClientAvecNomAlorsRetoureClient() {
        // given
        Client client = new Client();
        client.setClientName("toto");
        client.setEmail("toto@gmail.com");
        client.setClientStatut(1);
        testEntityManager.persistAndFlush(client);

        // when
        List<Client> trouver = clientRepository.findByClientNameContaining("toto");
        // then
        assertTrue(!trouver.isEmpty());
        Assert.assertEquals(trouver.get(0).getClientName(), client.getClientName());

    }

    @Test
    public void whenRecherhceUnClientAvecNomAlorsUneListVide() {
        // given
        String clientnom = "joe";
        // when
        List<Client> trouver = clientRepository.findByClientNameContaining(clientnom);
        // then

        Assert.assertTrue(trouver.isEmpty());
    }


    @Test
    public void whenUpdateUnClientAvecNomAlorsRetoureClientAvecNouveauStatut() {
        // given
        Client client = new Client();
        client.setClientName("toto");
        client.setEmail("toto@gmail.com");
        client.setClientStatut(1);
        testEntityManager.persistAndFlush(client);

        // when
        clientRepository.updateStatut(1L, 2);
        Optional<Client> trouver = clientRepository.findById(1L);
        // then
        Assert.assertEquals(2, trouver.get().getClientStatut().intValue());

    }

    @Test
    public void whenUpdateStatusUnClientQuiNexistPas() {
        // given
        Client client = new Client();
        // when
        clientRepository.updateStatut(50L, 2);
        Optional<Client> trouver = clientRepository.findById(50L);
        // then
        Assert.assertTrue(!trouver.isPresent());

    }
}