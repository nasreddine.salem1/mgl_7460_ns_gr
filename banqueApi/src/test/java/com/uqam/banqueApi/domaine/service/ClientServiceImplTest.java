package com.uqam.banqueApi.domaine.service;


import com.uqam.banqueApi.domaine.entite.Client;
import com.uqam.banqueApi.domaine.repository.ClientRepository;
import com.uqam.banqueApi.exception.ValidationException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ClientServiceImplTest {


    @InjectMocks
    ClientServiceImpl clientService;

    @Mock
    ClientRepository clientRepository;

    Client clientdemander = new Client();

    List<Client> clientList = new ArrayList<>();

    @Before
    public void init() {

        clientdemander.setClientId(0L);
        clientdemander.setClientName("toto");
        clientdemander.setEmail("toto@gmail.com");
        clientdemander.setClientStatut(1);

        clientList.add(clientdemander);
    }


    @Test
    public void testerLaCreationDunClientDoitRetourneUnClient() {

        when(clientRepository.save(Mockito.any(Client.class))).thenReturn(clientdemander);
        Client clientCreer = clientService.addClient(clientdemander);
        verify(clientRepository, times(1)).save(clientdemander);
        Assert.assertEquals(clientdemander.getClientId(), clientCreer.getClientId());
        Assert.assertEquals(clientdemander.getClientName(), clientCreer.getClientName());
        Assert.assertEquals(clientdemander.getClientStatut(), clientCreer.getClientStatut());
    }


    @Test
    public void testerLaCreationAvecUnNomNull() {
        try {
            Client clientD = new Client();
            Client clientCreer = clientService.addClient(clientD);
            Assert.fail("le test ne doit pas passer");
        } catch (ValidationException e) {
            Assert.assertEquals("msg.client.name.notNull", e.getMsg());
        }
    }

    @Test
    public void testerLaCreationAvecUnStatusNull() {
        try {
            clientdemander.setClientStatut(null);
            Client clientCreer = clientService.addClient(clientdemander);
            Assert.fail("le test ne doit pas passer");
        } catch (ValidationException e) {
            Assert.assertEquals("msg.client.statut.notNull", e.getMsg());
        }
    }

    @Test
    public void testerLaRecherhceDunClientDoitRetourneUnClient() {

        when(clientRepository.findByClientNameContaining(Mockito.any(String.class))).thenReturn(clientList);
        List<Client> clientTrouver = clientService.findClient(clientdemander);
        Assert.assertTrue(clientTrouver.get(0).equals(clientList.get(0)));

    }

    @Test
    public void testerLaRecherhceDunClientDoitRetourneUneListVide() {

        when(clientRepository.findByClientNameContaining(Mockito.any(String.class))).thenReturn(Collections.emptyList());
        List<Client> clientTrouver = clientService.findClient(clientdemander);
        Assert.assertTrue(clientTrouver.isEmpty());

    }


    @Test
    public void testerLaMiseAJourStatutClient() {
        clientdemander.setClientStatut(5);
        clientService.updateStatut(clientdemander);
        Mockito.doNothing().when(clientRepository).updateStatut(Mockito.any(Long.class), Mockito.any(Integer.class));
        when(clientRepository.findById(Mockito.any(Long.class))).thenReturn(Optional.of(clientdemander));
        Assert.assertEquals(5, clientRepository.findById(0L).get().getClientStatut().intValue());

    }

    @Test
    public void testerAffichageDesClientAvecProduitEnAttente() {
        when(clientRepository.findAllClientForProductStatusAttente()).thenReturn(clientList);
        List<Client> clientListTrouver = clientService.findAllClientWithProductAttente();
        Assert.assertEquals(1, clientListTrouver.size());
        Assert.assertEquals("toto", clientListTrouver.get(0).getClientName());
    }

}