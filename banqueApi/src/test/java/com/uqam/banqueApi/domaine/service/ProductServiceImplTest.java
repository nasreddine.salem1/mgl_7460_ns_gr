package com.uqam.banqueApi.domaine.service;

import com.uqam.api.rest.v1.ressource.BanqueProduitRessource;
import com.uqam.api.rest.v1.ressource.ProduitRessource.StatutEnum;
import com.uqam.banqueApi.domaine.entite.Client;
import com.uqam.banqueApi.domaine.entite.Product;
import com.uqam.banqueApi.domaine.entite.ProductEnum;
import com.uqam.banqueApi.domaine.repository.ClientRepository;
import com.uqam.banqueApi.domaine.repository.ProductRepository;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class ProductServiceImplTest {

    @InjectMocks
    ProductServiceImpl productService;

    @Mock
    ProductRepository productRepository;

    @Mock
    ClientRepository clientRepository;

    Client clientdemander = new Client();
    Set<Product> productSet = new HashSet<Product>();
    Product product1;
    Product product2;

    @Before
    public void init() {

        clientdemander.setClientId(0L);
        clientdemander.setClientName("toto");
        clientdemander.setEmail("toto@gmail.com");
        clientdemander.setClientStatut(1);

        product1 = new Product(1L, ProductEnum.CP.nomProduit, "ATTENTE", clientdemander);
        product2 = new Product(2L, ProductEnum.VI.nomProduit, "ATTENTE", clientdemander);
        productSet.add(product1);
        productSet.add(product2);

    }

    @Test
    public void testerLaffichageDesProduitDunClient() {
        List<Client> clients = new ArrayList<>();
        clients.add(clientdemander);
        when(clientRepository.findByClientNameContaining(Mockito.anyString())).thenReturn(clients);
        when(productRepository.findByClient(Mockito.any(Client.class))).thenReturn(productSet);
        Set<Product> products = productService.findAllProduct(clientdemander.getClientName());
        Assert.assertEquals(2, products.size());

    }

    @Test
    public void testerListeVideDesPeoduits() {
        Set<Product> products = productService.findAllProduct("bidon");
        Assert.assertEquals(0, products.size());

    }

    @Test
    public void testerLaListDesProduitExistant() {
        List<ProductEnum> productEnums = new ArrayList<>();
        productEnums.add(ProductEnum.CP);
        productEnums.add(ProductEnum.FI);
        productEnums.add(ProductEnum.MA);
        productEnums.add(ProductEnum.VI);
        List<Client> clients = new ArrayList<>();
        clients.add(clientdemander);
        when(clientRepository.findByClientNameContaining(Mockito.anyString())).thenReturn(clients);
        List<ProductEnum> trouver = productService.findAllBanqueProduct("toto");
        Assert.assertEquals(productEnums, trouver);
    }

    @Test
    public void testerLaListDesProduitVide() {
        List<ProductEnum> productEnums = new ArrayList<>();
        productEnums.add(ProductEnum.CP);
        productEnums.add(ProductEnum.FI);
        productEnums.add(ProductEnum.MA);
        productEnums.add(ProductEnum.VI);
        List<ProductEnum> trouver = productService.findAllBanqueProduct("bidon");
        Assert.assertEquals(0, trouver.size());
    }

    @Test
    public void testerLaSuscribtionProduit() {
        List<Client> clients = new ArrayList<>();
        clients.add(clientdemander);
        Product productExpected = new Product();
        productExpected.setProductStatus(StatutEnum.ACCEPT.name());
        productExpected.setClient(clientdemander);
        productExpected.setProductName(ProductEnum.VI.nomProduit);
        when(clientRepository.findByClientNameContaining(Mockito.anyString())).thenReturn(clients);
        when(productRepository.save(Mockito.any())).thenReturn(productExpected);
        Product product = productService.suscribeProduit("toto", 2);
        Assert.assertEquals(productExpected.getProductName(), product.getProductName());
    }

    @Test
    public void testerLaUnsuscribtionProduit() {
        List<Client> clients = new ArrayList<>();
        clients.add(clientdemander);
        Product productExpected = new Product();
        productExpected.setProductStatus(StatutEnum.ATTENTE.name());
        productExpected.setClient(clientdemander);
        productExpected.setProductName(ProductEnum.VI.nomProduit);
        when(clientRepository.findByClientNameContaining(Mockito.anyString())).thenReturn(clients);
        when(productRepository.findByClientAndProductName(Mockito.any(), Mockito.any())).thenReturn(productExpected);
        when(productRepository.save(Mockito.any())).thenReturn(productExpected);
        Product product = productService.unsuscribeProduit("toto", 2);
        Assert.assertEquals("TODELETE", product.getProductStatus());
    }

    @Test
    public void testerLaMisesAjourDesStatutProduit() {
        List<Client> clients = new ArrayList<>();
        clients.add(clientdemander);
        when(clientRepository.findByClientNameContaining(Mockito.anyString())).thenReturn(clients);
        when(productService.findAllProductForClientName("toto", 1)).thenReturn(product1);
        productService.addOrRejectProduct("toto", 1, StatutEnum.ACCEPT);
        Mockito.verify(productRepository, times(1)).save(product1);

    }
}