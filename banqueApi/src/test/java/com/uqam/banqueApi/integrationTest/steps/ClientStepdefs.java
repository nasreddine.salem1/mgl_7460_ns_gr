package com.uqam.banqueApi.integrationTest.steps;


import com.uqam.banqueApi.domaine.entite.Client;
import com.uqam.banqueApi.integrationTest.CucumberSpringContextConfiguration;
import io.cucumber.java.DataTableType;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import junit.framework.Assert;
import org.springframework.boot.test.web.client.TestRestTemplate;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;


public class ClientStepdefs extends CucumberSpringContextConfiguration {

    private TestRestTemplate testRestTemplate = new TestRestTemplate();
    private ResponseEntity<Client> clientResponseEntity;
    private List<Client> clientList;
    int statutCode = 0;
    int nomberInsert = 0;
    String msg = null;

    String baseUrl = "http://localhost:8787/api/v1";

    @DataTableType
    public Client definirClient(Map<String, String> entry) {
        Client client = new Client();
        client.setClientName(entry.get("clientName"));
        client.setEmail(entry.get("email"));
        client.setPhone("phone");
        client.setClientStatut(Integer.parseInt(entry.get("clientStatut")));
        return client;
    }

    @Given("employee veux ajouter un client avec ces informations:")
    public void employeeVeuxAjouterUnClientAvecCesInformations(List<Client> clients) {
        clientList = clients;
    }

    @When("employee sauvegarde le nouveau client avec les informatipn requise")
    public void employeeSauvegardeLeNouveauClientAvecLesInformatipnRequise() {
        for (Client client : clientList) {
            clientResponseEntity = testRestTemplate.postForEntity(baseUrl + "/employee", client, Client.class);
            if (!clientResponseEntity.getStatusCode().isError()) {
                statutCode = clientResponseEntity.getStatusCode().value();
                nomberInsert++;
            } else {
                statutCode = clientResponseEntity.getStatusCode().value();
                msg = clientResponseEntity.getStatusCode().series().toString();

            }
        }
    }


    @Then("la sauvegarde est succes avec le code {int}")
    public void laSauvegardeEstSuccesAvecLeCode(int code) {
        Assert.assertEquals(code, statutCode);
    }

    @And("le le nombre de client inserer egale a {int}")
    public void leLeNombreDeClientInsererEgaleA(int nombreInsertion) {
        Assert.assertEquals(nombreInsertion, nomberInsert);
    }


    @Then("on recois le code {int}")
    public void onRecoisLeCode(int code) {
        Assert.assertEquals(code, statutCode);
    }


    @And("est je recois le message : {string}")
    public void estJeRecoisLeMessage(String message) {
        Assert.assertEquals(message, msg);
    }


    @When("employee update le client avec le nouveeau statut : {int}")
    public void employeeUpdateLeClientAvecLeNouveeauStatut(int statut) {
        for (Client client : clientList) {
            client.setClientStatut(statut);

            HttpEntity<Client> request = new HttpEntity<>(clientList.get(0));
            ResponseEntity<Client> clientResponseEntity = testRestTemplate
                .exchange(baseUrl + "/employee/updateStatus", HttpMethod.PUT, request, Client.class);
            if (!clientResponseEntity.getStatusCode().isError()) {
                statutCode = clientResponseEntity.getStatusCode().value();
                nomberInsert++;
            } else {
                statutCode = clientResponseEntity.getStatusCode().value();
                msg = clientResponseEntity.getStatusCode().series().toString();

            }
        }

    }


}
