Feature: Ajout d'un nouveau client

  Scenario:   AVEC TOUS LES INFORMATION REQUISE EST SUCCESS

    Given employee veux ajouter un client avec ces informations:
      | clientName | email           | phone      | clientStatut |
      | test1      | test1@gmail.com | 5142144444 | 1            |
      | test2      | test2@gmail.com | 5142145555 | 4            |
    When employee sauvegarde le nouveau client avec les informatipn requise
    Then la sauvegarde est succes avec le code 200
    And le le nombre de client inserer egale a 2


  Scenario:   NOM DU CLIENT EST NULL

    Given employee veux ajouter un client avec ces informations:
      | clientName | email           | phone      | clientStatut |
      |            | test1@gmail.com | 5142144444 | 1            |
    When employee sauvegarde le nouveau client avec les informatipn requise
    Then  on recois le code 400
    And  est je recois le message : "CLIENT_ERROR"

  Scenario:   NOM DU CLIENT EXIST

    Given employee veux ajouter un client avec ces informations:
      | clientName | email           | phone      | clientStatut |
      | Iron man   | test1@gmail.com | 5142144444 | 1            |
    When employee sauvegarde le nouveau client avec les informatipn requise
    Then  on recois le code 400
    And  est je recois le message : "CLIENT_ERROR"


