package com.uqam.clientBanque;

import com.uqam.config.ApplicationContextProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ClientBanque {

    private static final String PATTERNLINE_CMD ="(.*\\s|)(?<cmd>(--status|--avail|--subscribe|--unsubscribe|exit))(\\s.*|$)";
    static final Logger LOG = LoggerFactory.getLogger(ClientBanque.class);

     public String[] execute(String cmdLine) {
         final String[] errorCmdLine = {"Ligne de commande erronée"};
         final String[] errorCmd = {"Commande erronée"};

         if(cmdLine==null || cmdLine.trim().length()==0)
             return errorCmdLine ;

         String cmdCode= getCmdCode(cmdLine);
         if(cmdCode==null)
             return errorCmd ;
        //Obtenir le cmd via factory bean
         CommandClient cmd = (CommandClient) ApplicationContextProvider.getApplicationContext().getBean(cmdCode);

          if(cmd.isValidCMD(cmdLine)){
               return cmd.executeCmd(cmdLine);
          }
          else{
              return errorCmd;
          }
     }

     public String getCmdCode(String cmdLine) {
          Pattern pattern = Pattern.compile(PATTERNLINE_CMD,Pattern.CASE_INSENSITIVE);
          Matcher matcherCmd= pattern.matcher(cmdLine);
          matcherCmd.find();

          if(matcherCmd.matches())
          {
              return  matcherCmd.group("cmd").replace("--","").toLowerCase();
          }
          else{
               //Aucune cmd trouvees
               return null;
          }
     }
}

