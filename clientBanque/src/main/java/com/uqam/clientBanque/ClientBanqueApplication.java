package com.uqam.clientBanque;

import com.uqam.config.ConfigurationClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;


@EnableAutoConfiguration
@SpringBootApplication
@Import(ConfigurationClient.class)
public class ClientBanqueApplication implements CommandLineRunner {
	private static final Logger LOG = LoggerFactory.getLogger(ClientBanqueApplication.class);

	public static void main(String[] args) {
	SpringApplication.run(ClientBanqueApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		if(args== null ||  args.length==0)
			return;

		StringBuilder bld = new StringBuilder();
		for (int i = 0; i < args.length; ++i) {
			bld.append(args[i]);
			bld.append(" ");
		}
		String cmdLine = bld.toString();
		start(cmdLine.trim());
	}

	public void start(String cmdLine)
	{
		LOG.info("*****************Reponse Banque cmd client***************");
		ClientBanque client = new ClientBanque();
		String[] results= client.execute(cmdLine);
		for (String result:results) {
			LOG.info(result);
		}
		System.exit(0);
	}

}


