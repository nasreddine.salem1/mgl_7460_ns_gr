package com.uqam.clientBanque;

import com.uqam.api.rest.v1.ressource.BanqueProduitRessource;
import com.uqam.api.rest.v1.ressource.ProduitRessource;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class CommandClient
{
        public static final String NOM_CLIENT_INVALIDE = "nom client invalide.";
        public static final String PRODUCTID_INVALIDE = "product id invalide.";
        public static final String UNE_ERREUR_S_EST_PRODUIT = "Une erreur s'est produit";
        public static final String NOM_CLIENT_INVALIDE_OR_PRODUCTID_INVALIDE = "nom client invalide ou product id invalide.";

        public abstract String[] executeCmd(String parameters);
        protected  abstract String getPatternCmd();

        public static Matcher extractParam(String pattern, String lineCmd)
        {
                Pattern r = Pattern.compile(pattern,Pattern.CASE_INSENSITIVE);
                Matcher matcher = r.matcher(lineCmd);
                matcher.find();
                return matcher;
        }

        public static  String getGrpValue(String pattern, String lineCmd, String grpName) {
                try{
                        Matcher matcher = extractParam(pattern,lineCmd) ;
                        return matcher.group(grpName).toString();
                }
                catch (Exception ex)
                {
                        return null;
                }
        }

        public static  String formatProduit(ProduitRessource produitRessource)
        {
                return String.format("%d %d %s %s", produitRessource.getId(), produitRessource.getClientId(),produitRessource.getNomProduit(),produitRessource.getStatut());
        }
        public static  String formatProduit(BanqueProduitRessource produitRessource)
{
        return String.format("%d %s", produitRessource.getId(), produitRessource.getProduitDescrition());
}
        public boolean isValidCMD(String parameters)
        {
                String patternToValidate = getPatternCmd();
                Pattern pattern = Pattern.compile(patternToValidate,Pattern.CASE_INSENSITIVE);
                return pattern.matcher(parameters).matches();
        }
}

