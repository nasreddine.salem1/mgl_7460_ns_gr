package com.uqam.clientBanque;

import com.uqam.api.rest.v1.ressource.AllAvailProduitRessource;
import com.uqam.api.rest.v1.ressource.BanqueProduitRessource;
import com.uqam.reste.api.client.ClientBanqueApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("avail")
public class CommandClientAvail extends CommandClient {

    private static final String PATTERN_CLIENT_AVAIL ="-n(?<name>\\s\\w+.*\\s)--avail$";

    @Autowired
    private ClientBanqueApi autowiredClientBanqueApi;

    @Override
    protected String getPatternCmd()
    {
        return PATTERN_CLIENT_AVAIL;
    }
    @Override
    public String[] executeCmd(String parameters) {

        String nomClient = getGrpValue(PATTERN_CLIENT_AVAIL,parameters,"name");
        if(nomClient ==null)
                return new String[]{NOM_CLIENT_INVALIDE};

            List<String> resultats = new ArrayList<String>();
            AllAvailProduitRessource produits = autowiredClientBanqueApi.avail(nomClient.trim().toLowerCase());
            for (BanqueProduitRessource produitRessource : produits) {
                String line  = formatProduit(produitRessource);
                resultats.add( line);
            }

        return resultats.toArray(new String[0]);
    }


}
