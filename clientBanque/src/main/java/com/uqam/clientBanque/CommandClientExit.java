package com.uqam.clientBanque;


import org.springframework.stereotype.Component;

@Component("exit")
public class CommandClientExit extends CommandClient {
    private static final String PATTERN_EXIT ="^exit$";

    @Override
    public String[] executeCmd(String parameters) {
        return new String[]{"BYE"};
    }

    @Override
    protected String getPatternCmd() {
        return PATTERN_EXIT;
    }
}
