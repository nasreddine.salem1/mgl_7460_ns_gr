package com.uqam.clientBanque;

import com.uqam.api.rest.v1.ressource.AllProduitRessource;
import com.uqam.api.rest.v1.ressource.ProduitRessource;
import com.uqam.reste.api.client.ClientBanqueApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("status")
public class CommandClientStatus extends CommandClient {

        @Autowired
        private ClientBanqueApi autowiredClientBanqueApi;

        private static final String PATTERN_LINE_CMD_STATUS  ="-n(?<name>\\s\\w+.*\\s)--status";


        @Override
        protected String getPatternCmd()
        {
                return PATTERN_LINE_CMD_STATUS;
        }


        @Override
        public String[] executeCmd(String parameters) {

                String nomClient = getGrpValue(PATTERN_LINE_CMD_STATUS,parameters,"name");
                List<String> resultats = new ArrayList<String>();
                resultats.add("Id ClientId NomProduit Statut");
                if(nomClient!=null) {
                        AllProduitRessource produits = autowiredClientBanqueApi.status(nomClient.trim().toLowerCase());
                        for (ProduitRessource produitRessource : produits) {
                                String line  = formatProduit(produitRessource);
                                resultats.add( line);
                        }
                }
               return resultats.toArray(new String[0]);
        }
}
