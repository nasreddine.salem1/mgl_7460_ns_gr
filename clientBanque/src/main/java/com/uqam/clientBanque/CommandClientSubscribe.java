package com.uqam.clientBanque;

import com.uqam.api.rest.v1.ressource.ProduitRessource;
import com.uqam.reste.api.client.ClientBanqueApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("subscribe")
public class CommandClientSubscribe extends CommandClient {

    @Autowired
    private ClientBanqueApi autowiredClientBanqueApi;

    private static final String PATTERN_LINE_CMD_SUBSCRIBE ="-n(?<name>\\s\\w+.*)\\s--subscribe\\s(?<productid>\\d*)$";

    @Override
    protected String getPatternCmd()
    {
        return PATTERN_LINE_CMD_SUBSCRIBE;

    }

    
    @Override
    public String[] executeCmd(String parameters) {


        String nomClient = getGrpValue(PATTERN_LINE_CMD_SUBSCRIBE ,parameters,"name");
        if(nomClient==null)
        {
           return new String[]{NOM_CLIENT_INVALIDE};
        }

        Long produitId;
        try {
            produitId = Long.parseLong(getGrpValue(PATTERN_LINE_CMD_SUBSCRIBE, parameters, "productid"));
        }
        catch (NumberFormatException ex)
        {
            return new String[]{PRODUCTID_INVALIDE};
        }


        ProduitRessource produit = new ProduitRessource();
        produit.setId(produitId);
        List<String> resultats = new ArrayList<String>();
        String mesage = autowiredClientBanqueApi.subscribe(nomClient.trim().toLowerCase(), produit);

        if (mesage != null) {
            resultats.add(mesage);
        } else {
            resultats.add(UNE_ERREUR_S_EST_PRODUIT);
        }

        return resultats.toArray(new String[0]);
    }

}