package com.uqam.clientBanque;

import com.uqam.api.rest.v1.ressource.ProduitRessource;
import com.uqam.reste.api.client.ClientBanqueApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("unsubscribe")
public class CommandClientUnsubscribe extends CommandClient {

    @Autowired
    private ClientBanqueApi autowiredClientBanqueApi;

    private static final String PATTERN ="-n(?<name>\\s\\w+.*)\\s--unsubscribe\\s(?<productid>\\d*)$";

    @Override
    protected String getPatternCmd()
    {
        return PATTERN;
    }

    @Override
    public String[] executeCmd(String parameters) {

      if(!super.isValidCMD(parameters))
          return new String[]{NOM_CLIENT_INVALIDE_OR_PRODUCTID_INVALIDE};

       String nomClient = getGrpValue(PATTERN,parameters,"name").trim().toLowerCase();

       Long produitId;
       try {
            produitId = Long.parseLong(getGrpValue(PATTERN, parameters, "productid"));
        }
        catch (NumberFormatException ex)
        {
            return new String[]{PRODUCTID_INVALIDE};
        }
        List<String> resultats = new ArrayList<String>();
        ProduitRessource produit = new ProduitRessource();
        produit.setId(produitId);
        String mesage = autowiredClientBanqueApi.unsubscribe(nomClient, produit);
        if (mesage != null) {
            resultats.add(mesage);
        } else {
            resultats.add(UNE_ERREUR_S_EST_PRODUIT);

        }

        return resultats.toArray(new String[0]);

    }
}
