package com.uqam.config;

import com.uqam.reste.api.client.ClientBanqueApi;
import com.uqam.reste.api.client.ClientBanqueApiTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.Ordered;

@Configuration
@PropertySource("classpath:application.properties")
@AutoConfigureOrder(Ordered.HIGHEST_PRECEDENCE)
public class ConfigurationClient {
    private static final Logger LOG = LoggerFactory.getLogger(ConfigurationClient.class);
    public ConfigurationClient(){
        LOG.info("-------------- ConfigurationClient config --------------");
    }

    @Bean
    public ClientBanqueApi autowiredClientBanqueApi() {
        return new ClientBanqueApiTemplate();

    }

    @Bean
    public ApplicationContextProvider autowiredApplicationContextProvider()
    {
        return new ApplicationContextProvider();
    }


}
