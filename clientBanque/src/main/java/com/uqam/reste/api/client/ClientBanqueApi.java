package com.uqam.reste.api.client;

import com.uqam.api.rest.v1.ressource.AllAvailProduitRessource;
import com.uqam.api.rest.v1.ressource.AllProduitRessource;
import com.uqam.api.rest.v1.ressource.ProduitRessource;
import org.springframework.stereotype.Component;

@Component
public interface ClientBanqueApi {
    AllAvailProduitRessource avail(String clientName);

    AllProduitRessource status(String clientName);

    String subscribe(String clientName, ProduitRessource produitid);

    String unsubscribe(String clientName, ProduitRessource produitid);
}
