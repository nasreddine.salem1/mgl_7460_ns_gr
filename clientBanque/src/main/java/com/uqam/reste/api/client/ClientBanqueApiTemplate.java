package com.uqam.reste.api.client;

import com.uqam.api.rest.v1.ressource.AllAvailProduitRessource;
import com.uqam.api.rest.v1.ressource.AllProduitRessource;
import com.uqam.api.rest.v1.ressource.ProduitRessource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
@Service
@PropertySource("classpath:application.properties")
public class ClientBanqueApiTemplate implements ClientBanqueApi {

    public static final String UNE_ERREUR_S_EST_PRODUIT_EN_RETIRANT_LE_PRODUIT = "Une erreur s'est produit en retirant le produit.";
    public static final String UNE_ERREUR_S_EST_PRODUIT_EN_AJOUTANT_LE_PRODUIT = "Une erreur s'est produit en ajoutant le produit (%d).";
    public static final String LE_PRODUIT_D_A_ETE_AJOUTE = "Le produit (%d) a ete ajoute";
    public static final String LE_PRODUIT_D_A_ETE_RETIRE = "Le produit (%d) a ete retire";

    //TODO: mettre dans des variable
    private String service="client";
    private String url="http://localhost:8787/api/v1";

    private static final Logger LOG = LoggerFactory.getLogger(ClientBanqueApiTemplate.class);


    public String getUrl(String clientName, String operation) {

        return String.format("%s/%s/%s/%s",  url , service ,   operation , clientName.trim());
    }
    public String getUrl(String clientName, String operation, Long produitid)  {

        return String.format("%s/%s/%s/%s/%d",  url , service ,   operation , clientName.trim(),produitid);
    }

    private AllProduitRessource getProduits(String clientName, String operation)
    {
        validateParam(clientName, operation);

       try {
            RestTemplate restTemplate = new RestTemplate();
            String urlapi = getUrl(clientName, operation);
            ResponseEntity<AllProduitRessource> result  = restTemplate.getForEntity(urlapi,AllProduitRessource.class);
            if(result.getStatusCode().is2xxSuccessful())
                return result.getBody();
            else
                return null;
        }
       catch (Exception ex)
       {
           LOG.error(ex.getMessage());
           return null;
       }
    }

    private  boolean setRequestProduit(String clientName, String operation, ProduitRessource produit, HttpMethod method)
    {
        validateParam(clientName, operation);

        validateBodyData(produit);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<ProduitRessource> requestEntity = new HttpEntity<ProduitRessource>(produit, headers);

        try {
            String urlapi = getUrl(clientName, operation,produit.getId());
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> response = restTemplate.exchange(urlapi, method,  requestEntity, String.class);
            return response.getStatusCode().is2xxSuccessful();

        }
        catch(Exception ex)
        {
            LOG.error(ex.getMessage());
            return false;
        }
    }

    private void validateBodyData(ProduitRessource produit) {
        if(produit==null)
            throw new NullPointerException("le produit est vide");
    }

    private void validateParam(String clientName, String operation) {
        if (clientName == null)
            throw new NullPointerException("Le nom clientName est vide");

        if (operation == null)
            throw new NullPointerException("L'operation n'existe pas");
    }

    @Override
    public AllAvailProduitRessource avail(String clientName)
    {
        final String  operation = "avail";
        validateParam(clientName, operation);

        RestTemplate restTemplate = new RestTemplate();
        try {
            String urlapi = getUrl(clientName, operation);
            ResponseEntity<AllAvailProduitRessource> result  = restTemplate.getForEntity(urlapi,AllAvailProduitRessource.class);
            if(result.getStatusCode().is2xxSuccessful())
                return result.getBody();
            else
                return null;
        }
        catch (Exception ex)
        {
            LOG.error(ex.getMessage());
            return null;
        }
    }

    @Override
    public AllProduitRessource status(String clientName)
    {
        final String  operation = "status";
        return getProduits(clientName,operation);

    }
    @Override
    public String subscribe(String clientName, ProduitRessource produitRessource)
    {
        final String  operation = "subscribe";

        if(setRequestProduit(clientName,operation,produitRessource,HttpMethod.POST))
            return  String.format(LE_PRODUIT_D_A_ETE_AJOUTE,produitRessource.getId());
        else
            return String.format(UNE_ERREUR_S_EST_PRODUIT_EN_AJOUTANT_LE_PRODUIT,produitRessource.getId());
    }

    @Override
    public String unsubscribe(String clientName, ProduitRessource produitRessource)
    {
        final String  operation = "unsubscribe";
        if(setRequestProduit(clientName,operation,produitRessource,HttpMethod.PUT))
            return  String.format(LE_PRODUIT_D_A_ETE_RETIRE,produitRessource.getId());
        else
            return String.format(UNE_ERREUR_S_EST_PRODUIT_EN_RETIRANT_LE_PRODUIT,produitRessource.getId());
    }
}
