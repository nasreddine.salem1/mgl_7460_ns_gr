package com.uqam.clientBanque;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.uqam.api.rest.v1.ressource.AllAvailProduitRessource;
import com.uqam.api.rest.v1.ressource.AllProduitRessource;
import com.uqam.api.rest.v1.ressource.ProduitRessource;
import com.uqam.config.ConfigurationClient;
import com.uqam.reste.api.client.ClientBanqueApiTemplate;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static junit.framework.TestCase.assertEquals;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = ClientBanqueApplication.class)
@ContextConfiguration(classes = ConfigurationClient.class)
public class ClientBanqueApiTemplateTest {

    public static final String LE_PRODUIT_7_A_ETE_AJOUTE = "Le produit (7) a ete ajoute";
    public static final String LE_PRODUIT_EST_VIDE = "le produit est vide";
    public static final String LE_NOM_CLIENT_NAME_EST_VIDE = "Le nom clientName est vide";
    public static final String LE_PRODUIT_7_A_ETE_RETIRE = "Le produit (7) a ete retire";
    private static   WireMockServer wm = new WireMockServer(options().port(8787));

    @Autowired
    ClientBanqueApiTemplate  api;

    @BeforeClass
    public  static void Init()
    {
        //-- Avail
        wm.stubFor(get(urlEqualTo("/api/v1/client/avail/client_name"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-Type", APPLICATION_JSON_VALUE)
                        .withBody("[{\"id\": 99,\"clientId\": 0,\"nomProduit\": \"Test integre\",\"statut\": \"accept\"}]")));
        //subscribe
        wm.stubFor(post(urlEqualTo("/api/v1/client/subscribe/clientajout/7"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.ACCEPTED.value())
                        .withBody("\"successful operation\"")
                )
        );
        // --Status
        wm.stubFor(get(urlEqualTo("/api/v1/client/status/client_name"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-Type", APPLICATION_JSON_VALUE)
                        .withBody("[{\"id\": 99,\"clientId\": 0,\"nomProduit\": \"Test integre\",\"statut\": \"accept\"},{\"id\": 100,\"clientId\": 0,\"nomProduit\": \"Test integre\",\"statut\": \"accept\"}]")));


        wm.stubFor(put(urlEqualTo("/api/v1/client/unsubscribe/clientretire/7"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.ACCEPTED.value())
                        .withBody("\"successful operation\"")
                )
        );

        wm.stubFor(put(urlEqualTo("/api/v1/client/unsubscribe/Erreur/7"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.FORBIDDEN.value())
                        .withBody("\"erreur operation\"")
                )
        );
        wm.stubFor(post(urlEqualTo("/api/v1/client/subscribe/Erreur/7"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.FORBIDDEN.value())
                        .withBody("\"erreur operation\"")
                )
        );
        wm.start();

    }

    @AfterClass
    public  static  void After()
    {
        wm.stop();
    }
    @Test
    public void getUrlForAvailApiTest()
    {
        ClientBanqueApiTemplate  api = new ClientBanqueApiTemplate();
        String expected = api.getUrl("GR","avail" ).toString();
        assertEquals("http://localhost:8787/api/v1/client/avail/GR",expected);

    }

    @Test
    public void getUrlForStatusApiTest()
    {
        String expected = api.getUrl("GR","status" ).toString();
        assertEquals("http://localhost:8787/api/v1/client/status/GR",expected);

    }

    @Test
    public void getUrlForSubscribeApiTest()
    {
        String expected = api.getUrl("GR","subscribe", (long) 7).toString();
        assertEquals("http://localhost:8787/api/v1/client/subscribe/GR/7",expected);

    }
    @Test
    public void getAvailApi()
    {
        final int expectedCountProduit = 1;
        AllAvailProduitRessource result = api.avail("CLIENT_NAME".toLowerCase());
        assertEquals( expectedCountProduit, result.size());

    }

    @Test
    public void getStatusApi()
    {
        final int expectedCountProduit = 2;
        AllProduitRessource result = api.status("CLIENT_NAME".toLowerCase());
        assertEquals(expectedCountProduit,result.size());

    }

    @Test
    public void getSubscribeApi()
    {
        ProduitRessource produit =new ProduitRessource();
        produit.setId(Long.parseLong("7"));
        produit.setClientId(Long.parseLong("5544111"));
        produit.setNomProduit("nomProduit1");
        produit.setStatut(ProduitRessource.StatutEnum.ACCEPT);
        String result = api.subscribe("ClientAjout".toLowerCase(),produit);
        assertEquals(LE_PRODUIT_7_A_ETE_AJOUTE, result);

    }
    @Test
    public void getUnsubscribeApi()
    {
        ProduitRessource produit =new ProduitRessource();
        produit.setId(Long.parseLong("7"));
        produit.setClientId(Long.parseLong("5544111"));
        produit.setNomProduit("nomProduit1");
        produit.setStatut(ProduitRessource.StatutEnum.ACCEPT);
        String result = api.unsubscribe("ClientRetire".toLowerCase(),produit);
        assertEquals(LE_PRODUIT_7_A_ETE_RETIRE, result);

    }
    @Test
    public void getSubscribeErreurApi()
    {
        ProduitRessource produit =new ProduitRessource();
        produit.setId(Long.parseLong("7"));
        produit.setClientId(Long.parseLong("5544111"));
        produit.setNomProduit("nomProduit1");
        produit.setStatut(ProduitRessource.StatutEnum.ACCEPT);
        String result = api.subscribe("Erreur",produit);
        String expected = String.format(ClientBanqueApiTemplate.UNE_ERREUR_S_EST_PRODUIT_EN_AJOUTANT_LE_PRODUIT,7);
        assertEquals(expected, result);
    }
    @Test
    public void getUnsubscribeErreurApi()
    {
        ProduitRessource produit =new ProduitRessource();
        produit.setId(Long.parseLong("7"));
        produit.setClientId(Long.parseLong("5544111"));
        produit.setNomProduit("nomProduit1");
        produit.setStatut(ProduitRessource.StatutEnum.ACCEPT);
        String result = api.unsubscribe("Erreur",produit);
        assertEquals(ClientBanqueApiTemplate.UNE_ERREUR_S_EST_PRODUIT_EN_RETIRANT_LE_PRODUIT, result);
    }
    @Test
    public void getSubscribeApiSansProduit()
    {
        try
        {
            String result = api.subscribe("CLIENT_NAME",null);
            Assert.fail();
        }
        catch (NullPointerException ex)
        {
            assertEquals(LE_PRODUIT_EST_VIDE,ex.getMessage().toString());

        }

    }

    @Test
    public void getSubscribeApiSansNom()
    {
        try
        {
            String result = api.subscribe(null,null);
            Assert.fail();
        }
        catch (NullPointerException ex)
        {
            assertEquals(LE_NOM_CLIENT_NAME_EST_VIDE,ex.getMessage().toString() );

        }

    }
}
