package com.uqam.clientBanque;

import com.uqam.config.ConfigurationClient;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ClientBanqueApplication.class)
@ContextConfiguration(classes = ConfigurationClient.class)
public class ClientBanqueApplicationTest {

    @Test
    public void runTestEmpty()
    {
        try {
            CommandLineRunner app = new ClientBanqueApplication();
            app.run(new String[0]);
        }
      catch (Exception ex)
      {
          Assert.fail();}
    }
    @Test
    public void runArgsEmptyTest() {
        CommandLineRunner app = new ClientBanqueApplication();
        try {
            app.run(new String[]{});

        } catch (Exception e) {
            Assert.fail();
        }
    }
}
