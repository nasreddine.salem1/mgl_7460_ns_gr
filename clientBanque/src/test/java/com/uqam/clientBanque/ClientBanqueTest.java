package com.uqam.clientBanque;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.uqam.config.ConfigurationClient;
import com.uqam.reste.api.client.ClientBanqueApi;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.junit.Assert.assertEquals;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ClientBanqueApplication.class)
@ContextConfiguration(classes = ConfigurationClient.class)
public class ClientBanqueTest {

    public static final String LIGNE_DE_COMMANDE_ERRONÉE = "Ligne de commande erronée";
    public static final String COMMANDE_ERRONÉE = "Commande erronée";
    @Spy
    private ClientBanqueApi apiClient;
    @InjectMocks
    private ClientBanque testingObject;

    private WireMockServer wm = new WireMockServer(options().port(8787));
    @Before
    public void Init()
    {
        //-- Avail
        wm.stubFor(get(urlEqualTo("/api/v1/client/avail/client"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-Type", APPLICATION_JSON_VALUE)
                        .withBody("[{\"id\": 99,\"produitDescrition\": \"Test integre\"}]")));
        wm.start();

    }

    @After
    public void After()
    {
        wm.stop();
    }

    @Test
    public void executeNullTest()
    {
        String cmdLine=null;

        String[] resultat= testingObject.execute(cmdLine);
        Assert.assertEquals(LIGNE_DE_COMMANDE_ERRONÉE,resultat[0]);


    }
    @Test
    public void executeVideTest()
    {
        String cmdLine="";

        String[] resultat= testingObject.execute(cmdLine);
        Assert.assertEquals(LIGNE_DE_COMMANDE_ERRONÉE,resultat[0]);


    }
    @Test
    public void executeBadCmdTest()
    {
        String cmdLine="dakdaksdkasdjasbdjasbdjsabd";

        String[] resultat= testingObject.execute(cmdLine);
        Assert.assertEquals(COMMANDE_ERRONÉE,resultat[0]);
    }
    @Test
    public void executeCmdInexistant()
    {
        String cmdLine="-n CLIENT  NAME --mep";
        String[] resultat= testingObject.execute(cmdLine);
        Assert.assertEquals(COMMANDE_ERRONÉE,resultat[0]);
    }
    @Test
    public void getCmdCodeStatus()
    {
        String cmdLine="-n Test Status --status";
        String code= testingObject.getCmdCode(cmdLine);
        Assert.assertEquals("status",code);


    }
    @Test
    public void getCmdCodeAvail()
    {
        String cmdLine="-n Test Status --avail";

        String code= testingObject.getCmdCode(cmdLine);
        Assert.assertEquals("avail",code);


    }

    @Test
    public void getCmdCodeSubscribe()
    {
        String cmdLine="-n GR --subscribe 14";
        String code= testingObject.getCmdCode(cmdLine);
        Assert.assertEquals("subscribe",code);
    }

    @Test
    public void getCmdCodeUnsubscribe()
    {
        String cmdLine="-n GR --unsubscribe 14";
        String code= testingObject.getCmdCode(cmdLine);
        Assert.assertEquals("unsubscribe",code);
    }

    @Test
    public void executeCmd()
    {

        String[] result = testingObject.execute("-n CLIENT --avail");
        assertEquals("99 Test integre",result[0].trim() );

    }
}
