package com.uqam.clientBanque;

import com.uqam.api.rest.v1.ressource.AllAvailProduitRessource;
import com.uqam.api.rest.v1.ressource.BanqueProduitRessource;
import com.uqam.reste.api.client.ClientBanqueApi;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

    @RunWith(MockitoJUnitRunner.class)
    public class CommandClientAvailTest {


    @InjectMocks
    private CommandClientAvail testingObject;

    @Spy
    private ClientBanqueApi apiClient;

   @Test
    public void valideParametersGoodParam()
    {
        CommandClientAvail  cmd = new CommandClientAvail();
        boolean result = cmd.isValidCMD("-n CLIENT_NAME --avail");
        assertTrue(result);
    }

    @Test
    public void valideParametersBadParam()
    {

        CommandClientAvail  cmd = new CommandClientAvail();
        boolean result = cmd.isValidCMD("-n CLIENT_NAME --ava");
        assertFalse(result);

    }

    @Test
    public void executeCmd()
    {

        BanqueProduitRessource produit =new BanqueProduitRessource();
        produit.setId(Long.parseLong("7"));
        produit.setProduitDescrition("Carte Credit");
        AllAvailProduitRessource resultats_mocks = new AllAvailProduitRessource();
        resultats_mocks.add(produit);
        String lineExpected  = String.format("%d %s", produit.getId(), produit.getProduitDescrition());

        Mockito.doReturn(resultats_mocks).when(apiClient).avail("client name");
        String[] result = testingObject.executeCmd("-n CLIENT NAME --avail");
        assertEquals(lineExpected.trim(),result[0].trim() );

    }


        @Test
        public void executeCmdNomClientInvalide()
        {
          String[] result = testingObject.executeCmd("-n --avail");
          assertEquals(CommandClientAvail.NOM_CLIENT_INVALIDE  ,result[0].trim() );

        }
}
