package com.uqam.clientBanque;

import com.uqam.config.ConfigurationClient;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ClientBanqueApplication.class)
@ContextConfiguration(classes = ConfigurationClient.class)
public class CommandClientExitTest {


    @Test
    public void cmdExitTest()
    {
        CommandClientExit cmd = new CommandClientExit();
        String[] result =  cmd.executeCmd("exit");
        Assert.assertEquals("BYE",result[0]);

    }

    @Test
    public void getPatterntest()
    {
        CommandClientExit cmd = new CommandClientExit();
        Assert.assertEquals("^exit$",cmd.getPatternCmd());
    }
}
