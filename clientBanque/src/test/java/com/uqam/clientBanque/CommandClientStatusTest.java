package com.uqam.clientBanque;
import com.uqam.api.rest.v1.ressource.AllProduitRessource;
import com.uqam.api.rest.v1.ressource.ProduitRessource;
import com.uqam.reste.api.client.ClientBanqueApi;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(MockitoJUnitRunner.class)
public class CommandClientStatusTest {

    @InjectMocks
    private CommandClientStatus testingObject;

    @Spy
    private ClientBanqueApi apiClient;

    @Test
    public void valide_parameters_good_param()
    {
        CommandClientStatus  cmd = new CommandClientStatus();
        boolean result = cmd.isValidCMD("-n CLIENT_NAME --status");
        assertTrue(result);
    }

    @Test
    public void valide_parameters_bad_param()
    {
        CommandClientStatus  cmd = new CommandClientStatus();
        boolean result = cmd.isValidCMD("-n CLIENT_NAME --sta");
        assertFalse(result);

    }

    @Test( )
    public void executeCmd()
    {
        AllProduitRessource resultats_mocks = new AllProduitRessource();
        ProduitRessource produit =new ProduitRessource();
        produit.setId(Long.parseLong("7"));
        produit.setClientId(Long.parseLong("5544111"));
        produit.setNomProduit("nomProduit1");
        produit.setStatut(ProduitRessource.StatutEnum.ACCEPT);

        resultats_mocks.add(produit);

        ProduitRessource produit2 =new ProduitRessource();
        produit.setId(Long.parseLong("2"));
        produit.setClientId(Long.parseLong("5544111"));
        produit.setNomProduit("nomProduit2");
        produit.setStatut(ProduitRessource.StatutEnum.ACCEPT);
        resultats_mocks.add(produit2);

        Mockito.doReturn(resultats_mocks).when(apiClient).status("test status");
        String[] result = testingObject.executeCmd("-n Test Status --status");

        String lineExpected1  = String.format("%d %d %s %s", produit.getId(), produit.getClientId(),produit.getNomProduit(),produit.getStatut());
        String lineExpected2  = String.format("%d %d %s %s", produit2.getId(), produit2.getClientId(),produit2.getNomProduit(),produit2.getStatut());

        int expected = 3;
        assertEquals(expected,result.length );
        assertEquals( lineExpected1.trim(),result[1].trim());
        assertEquals( lineExpected2.trim(),result[2].trim());

    }
}
