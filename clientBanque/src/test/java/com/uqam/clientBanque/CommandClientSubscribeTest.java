package com.uqam.clientBanque;

import com.uqam.api.rest.v1.ressource.ProduitRessource;
import com.uqam.reste.api.client.ClientBanqueApi;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
@RunWith(MockitoJUnitRunner.class)
public class CommandClientSubscribeTest {

    @InjectMocks
    private CommandClientSubscribe testingObject;

    @Spy
    private ClientBanqueApi apiClient;

    @Test
    public void valide_parameters_good_param()
    {
        CommandClient  cmd = new CommandClientSubscribe();
        boolean result = cmd.isValidCMD("-n CLIENT_NAME --subscribe 1");
        assertTrue(result);
    }

    @Test
    public void valide_parameters_bad_param()
    {
        CommandClient  cmd = new CommandClientSubscribe();
        boolean result = cmd.isValidCMD("-n CLIENT_NAME --subribe PRODUCT_ID");
        assertFalse(result);

    }
    @Test
    public void valideParametersBadProduitIdTest()
    {
        CommandClient  cmd = new CommandClientSubscribe();
        String[] result = cmd.executeCmd("-n CLIENT_NAME --subscribe A");
        assertEquals(CommandClient.NOM_CLIENT_INVALIDE, result[0]);

    }

    @Test
    public void valideParametersBadNomTest()
    {
        CommandClient  cmd = new CommandClientSubscribe();
        String[] result = cmd.executeCmd("-n --subscribe A");
        assertEquals(CommandClient.NOM_CLIENT_INVALIDE, result[0]);

    }
    @Test
    public void executeCmd()
    {

        ProduitRessource produit =new ProduitRessource();
        produit.setId(Long.parseLong("10"));

        String messageExpected  = String.format("Le produit (%d) a ete ajoute",produit.getId());
        Mockito.doReturn(messageExpected).when(apiClient).subscribe("gr",produit);
        String[] result = testingObject.executeCmd("-n GR --subscribe 10");

        assertEquals(messageExpected, result[0]);


    }
    @Test
    public void executeCmdErreurApiTest()
    {

        ProduitRessource produit =new ProduitRessource();
        produit.setId(Long.parseLong("10"));

        Mockito.doReturn(null).when(apiClient).unsubscribe("gr",produit);
        String[] result = testingObject.executeCmd("-n GR --subscribe 10");
        assertEquals(CommandClient.UNE_ERREUR_S_EST_PRODUIT ,result[0]);


    }

}