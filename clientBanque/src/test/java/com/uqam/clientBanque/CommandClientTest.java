package com.uqam.clientBanque;

import com.uqam.api.rest.v1.ressource.BanqueProduitRessource;
import com.uqam.api.rest.v1.ressource.ProduitRessource;
import com.uqam.config.ConfigurationClient;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.regex.Matcher;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ClientBanqueApplication.class)
@ContextConfiguration(classes = ConfigurationClient.class)
public class CommandClientTest {


    @Test
    public void extractParamTest()
    {
        Matcher match= CommandClient.extractParam("-n(?<name>\\s\\w+.*\\s)--avail$","-n CLIENT_NAME --avail");
        Assert.assertTrue(match.matches());
    }

    @Test
    public void extractParamErrorTest()
    {
        String nomClient= CommandClient.getGrpValue("-n(?<name>\\s\\w+.*\\s)--avail$","-n CLIENT_NAME --avail","name");
        Assert.assertEquals("CLIENT_NAME",nomClient.trim());
    }
    @Test
    public void formatProduitTest()
    {
       ProduitRessource produitRessource =new ProduitRessource();
        produitRessource.setId(Long.parseLong("10"));
        produitRessource.setClientId(Long.parseLong("1"));
        produitRessource.setNomProduit("PR1");
        produitRessource.setStatut(ProduitRessource.StatutEnum.ACCEPT);

        String expected =String.format("%d %d %s %s", 10, 1,"PR1","accept");

        Assert.assertEquals(expected,  CommandClient.formatProduit(produitRessource));
    }
    @Test
    public void formatBanqueProduitRessourceTest()
    {
        BanqueProduitRessource produitRessource =new BanqueProduitRessource();
        produitRessource.setId(Long.parseLong("1"));
        produitRessource.setProduitDescrition("PR1");


        String expected = String.format("%d %s", 1,"PR1") ;

        Assert.assertEquals(expected,  CommandClient.formatProduit(produitRessource));
    }


}


