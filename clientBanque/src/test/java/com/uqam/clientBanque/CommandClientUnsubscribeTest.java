package com.uqam.clientBanque;

import com.uqam.api.rest.v1.ressource.ProduitRessource;
import com.uqam.reste.api.client.ClientBanqueApi;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(MockitoJUnitRunner.class)
public class CommandClientUnsubscribeTest {

    @InjectMocks
    private CommandClientUnsubscribe testingObject;

    @Spy
    private ClientBanqueApi apiClient;

    @Test
    public void valideParametersGoodParamTest()
    {
        CommandClient  cmd = new CommandClientUnsubscribe();
        boolean result = cmd.isValidCMD("-n CLIENT_NAME --unsubscribe 1");
        assertTrue(result);
    }
    @Test
    public void valideParametersGoodParamUppercaseTest()
    {
        CommandClient  cmd = new CommandClientUnsubscribe();
        boolean result = cmd.isValidCMD("-n CLIENT_NAME --unsubscrIBE 1");
        assertTrue(result);
    }
    @Test
    public void valideParametersBadParamTest()
    {
        CommandClient  cmd = new CommandClientUnsubscribe();
        boolean result = cmd.isValidCMD("-n CLIENT_NAME --subribe 1");
        assertFalse(result);

    }

    @Test
    public void valideParametersBadProduitIdTest()
    {
        CommandClient  cmd = new CommandClientUnsubscribe();
        String[] result = cmd.executeCmd("-n CLIENT_NAME --unsubscribe");
        assertEquals(CommandClient.NOM_CLIENT_INVALIDE_OR_PRODUCTID_INVALIDE, result[0]);

    }

    @Test
    public void executeCmdTest()
    {

        ProduitRessource produit =new ProduitRessource();
        produit.setId(Long.parseLong("10"));
        String messageExpected  = String.format("Le produit (%d) a ete retire",produit.getId());
        Mockito.doReturn(messageExpected).when(apiClient).unsubscribe("gr",produit);
        String[] result = testingObject.executeCmd("-n GR --unsubscribe 10");
        assertEquals(messageExpected,result[0]);


    }
    @Test
    public void executeCmdErreurApiTest()
    {

        ProduitRessource produit =new ProduitRessource();
        produit.setId(Long.parseLong("10"));
        produit.setStatut(ProduitRessource.StatutEnum.ACCEPT);
        Mockito.doReturn(null).when(apiClient).unsubscribe("gr",produit);
        String[] result = testingObject.executeCmd("-n GR --unsubscribe 10");
        assertEquals(CommandClient.UNE_ERREUR_S_EST_PRODUIT ,result[0]);


    }
}
