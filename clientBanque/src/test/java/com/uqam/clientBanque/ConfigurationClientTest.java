package com.uqam.clientBanque;

import com.uqam.config.ApplicationContextProvider;
import com.uqam.config.ConfigurationClient;
import com.uqam.reste.api.client.ClientBanqueApi;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class ConfigurationClientTest {

    @Test
    public void getBeanClientBanqueApi()
    {
        ConfigurationClient config= new ConfigurationClient();
        ClientBanqueApi api = config.autowiredClientBanqueApi();
        assertNotNull(api);
    }
    @Test
    public void getBeanApplicationContextProvider()
    {
        ConfigurationClient config= new ConfigurationClient();
        ApplicationContextProvider app = config.autowiredApplicationContextProvider();
        assertNotNull(app);
    }
}
