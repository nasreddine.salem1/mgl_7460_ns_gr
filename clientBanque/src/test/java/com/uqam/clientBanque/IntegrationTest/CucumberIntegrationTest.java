package com.uqam.clientBanque.IntegrationTest;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:features",
        glue = {"com.uqam.clientBanque.IntegrationTest.steps"},
        plugin = {"pretty", "json:build/cucumber/cucumber.json", "junit:build/cucumber/cucumber.xml", "html:build/cucumber/html"})
public class CucumberIntegrationTest extends CucumberSpringContextConfiguration {

}
