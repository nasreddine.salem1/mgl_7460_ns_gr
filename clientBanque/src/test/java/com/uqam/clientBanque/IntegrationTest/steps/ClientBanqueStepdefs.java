package com.uqam.clientBanque.IntegrationTest.steps;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.uqam.clientBanque.ClientBanque;
import com.uqam.clientBanque.IntegrationTest.CucumberSpringContextConfiguration;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.http.HttpStatus;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

public class ClientBanqueStepdefs extends CucumberSpringContextConfiguration {
    private  WireMockServer wm = new WireMockServer(options().port(8787));
    private String commande_entree= "";
    private String[] produits;

    @Before
    public void Init()
    {

        //-- Avail
        wm.stubFor(get(urlEqualTo("/api/v1/client/avail/client_name"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-Type", APPLICATION_JSON_VALUE)
                        .withBody("[{\"id\": 99,\"clientId\": 0,\"nomProduit\": \"Test integre\",\"statut\": \"accept\"}]")));
        // --Status
        wm.stubFor(get(urlEqualTo("/api/v1/client/status/client_name"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-Type", APPLICATION_JSON_VALUE)
                        .withBody("[{\"id\": 99,\"clientId\": 0,\"nomProduit\": \"Test integre\",\"statut\": \"accept\"},{\"id\": 100,\"clientId\": 0,\"nomProduit\": \"Test integre\",\"statut\": \"accept\"}]")));

        //subscribe
        wm.stubFor(put(urlEqualTo("/api/v1/client/subscribe/client_name"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.ACCEPTED.value())
                        .withBody("\"successful operation\"")
                )
        );

        //subscribe
        wm.stubFor(put(urlEqualTo("/api/v1/client/subscribe/client_name"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.ACCEPTED.value())
                        .withBody("\"successful operation\"")
                )
        );
        wm.start();

    }
    @After
    public void Clean()
    {
        wm.stop();
    }
    @Given("que le client saisi la commande suivante : {string}")
    public void queLeClientSaisiLaCommandeSuivante(String arg0) {
        commande_entree=arg0.trim();
    }

    @When("le client appuie sur enter:")
    public void leClientAppuieSurEnter() {
        ClientBanque clientBanque = new ClientBanque();
        produits =   clientBanque.execute(commande_entree);
    }

    @Then("la requete affiche resultat suivant:")
    public void laRequeteAfficheResultatSuivant() {
        for (String produit:produits)
            System.out.println(produit);

    }

    @And("le nombre des produits retournes: {int}")
    public void leNombreDesProduitsRetournes(int arg0) {
        assertTrue(produits.length==arg0);

        if(commande_entree.trim().length()==0)
        {

            assertEquals("Ligne de commande erronée",produits[0]);
        }
    }
}
