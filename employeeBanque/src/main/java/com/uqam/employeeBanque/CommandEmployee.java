package com.uqam.employeeBanque;

public interface CommandEmployee {

    public  String[] executeCmd(String lineCmd);
}
