package com.uqam.employeeBanque;

import com.uqam.api.rest.v1.ressource.ProduitRessource;
import com.uqam.employeeBanque.reste.api.employee.EmployeBanqueApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component("accept")
public class CommandEmployeeAccept extends CommandEmployeeGestionProduit implements CommandEmployee{


    private static String patternlinecmdaccept ="^--accept(?<produitid>\\s\\d*)\\s*--client(?<client>(\\s\\w+|\\s\\w+\\s\\w+))$";
    @Autowired
    private EmployeBanqueApi clientEmployeBanqueApi;


    @Override
    public String[] executeCmd(String lineCmd) {

        if(!super.valide( lineCmd, patternlinecmdaccept))
        {
            return new String[] {"Erreur commande."};
        }

        String client =  super.getClient().toLowerCase();
        Long prouitid= Long.parseLong(super.getProuitid());

        ProduitRessource produitRessource = new ProduitRessource();
        produitRessource.setId(prouitid);

        ResponseEntity<String> response =  clientEmployeBanqueApi.accept(produitRessource,client);
        if (response.getStatusCode().is2xxSuccessful()) {
             return new String[] {response.getBody()};
        }
        else
            return new String[]{Objects.requireNonNull(response.getHeaders().get("Custom-Error")).toString()};


    }
}
