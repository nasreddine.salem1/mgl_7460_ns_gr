package com.uqam.employeeBanque;

import com.uqam.api.rest.v1.ressource.ClientRessource;
import com.uqam.employeeBanque.reste.api.employee.EmployeBanqueApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component("add")
public class CommandEmployeeAdd implements CommandEmployee{

    private static String linecmdadd ="^--add(?<name>(\\s\\w+|\\s\\w+\\s\\w+))$";
    @Autowired
    private EmployeBanqueApi clientEmployeBanqueApi;

    public ClientRessource getClient(String name)
    {
        ClientRessource client = new ClientRessource();
        client.setClientId((long) 0);
        client.setClientName(name.trim().toLowerCase());
        client.setEmail("");
        client.setClientStatut(1);
        client.setPhone("");
        return client;

    }

    @Override
    public  String[] executeCmd(String lineCmd)
    {
        Pattern pattern = Pattern.compile(linecmdadd,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(lineCmd.trim());
        if(!matcher.find())
        {
            return new String[] {"Erreur commande."};
        }

        ClientRessource client =  getClient(matcher.group("name"));
        ResponseEntity<ClientRessource>  clientResponse =  clientEmployeBanqueApi.add(client);

        if (clientResponse.getStatusCode().is2xxSuccessful()) {
           client = clientResponse.getBody();
           String newClient =  String.format("%d %s %s %s", client.getClientId(),client.getClientName(),client.getEmail(),client.getClientStatut());
           return new String[] {newClient};
        }
        else
            return new String[]{Objects.requireNonNull(clientResponse.getHeaders().get("Custom-Error")).toString()};

    }

}
