package com.uqam.employeeBanque;

import org.springframework.stereotype.Component;

@Component("exit")
public class CommandEmployeeExit implements CommandEmployee{

    @Override
    public String[] executeCmd(String lineCmd) {
        return new String[]{"Bye"};
    }
}
