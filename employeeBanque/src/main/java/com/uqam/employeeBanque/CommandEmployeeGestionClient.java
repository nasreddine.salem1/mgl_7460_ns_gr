package com.uqam.employeeBanque;

import com.uqam.api.rest.v1.ressource.ClientRessource;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandEmployeeGestionClient {

    private ClientRessource client;

    public boolean valide(String lineCmd, String patterncmd) {
        Pattern pattern = Pattern.compile(patterncmd, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(lineCmd.trim());
        if (matcher.find()) {
            client = new ClientRessource();
           client.setClientName(matcher.group("client").trim());
           return true;
        }
        return  false;
    }

    public ClientRessource getClient() {
        return client;
    }
}
