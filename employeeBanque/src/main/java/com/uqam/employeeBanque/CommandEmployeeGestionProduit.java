package com.uqam.employeeBanque;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandEmployeeGestionProduit {

    private String client;
    private String prouitid;


    public boolean valide(String lineCmd, String patterncmd) {
        Pattern pattern = Pattern.compile(patterncmd, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(lineCmd.trim());
        if (matcher.find()) {
            client = matcher.group("client").trim();
            prouitid = matcher.group("produitid").trim();
            return true;
        }
        return  false;
    }

    public String getClient() {
        return client;
    }

    public String getProuitid() {
        return prouitid;
    }
}
