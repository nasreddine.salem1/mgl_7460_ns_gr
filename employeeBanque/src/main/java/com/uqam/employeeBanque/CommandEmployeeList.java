package com.uqam.employeeBanque;

import com.uqam.api.rest.v1.ressource.AllProduitRessource;
import com.uqam.api.rest.v1.ressource.ProduitRessource;
import com.uqam.employeeBanque.reste.api.employee.EmployeBanqueApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component("list")
public class CommandEmployeeList implements CommandEmployee{
    static final Logger LOG = LoggerFactory.getLogger(CommandEmployeeList.class);

    private static String linecmdadd ="^--list(?<name>(\\s\\w+|\\s\\w+\\s\\w+))$";
    @Autowired
    private EmployeBanqueApi clientEmployeBanqueApi;


    @Override
    public  String[] executeCmd(String lineCmd)
    {
        Pattern pattern = Pattern.compile(linecmdadd,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(lineCmd.trim());
        if(!matcher.find())
        {
            return new String[] {"Erreur commande."};
        }

        String client = matcher.group("name");
        ResponseEntity<AllProduitRessource>  response=  clientEmployeBanqueApi.list(client.trim());

        if (response.getStatusCode().is2xxSuccessful()) {
            List<String> produits = new ArrayList<String>();
            for (ProduitRessource produit:response.getBody()) {
                produits.add(String.format("id:(%d) NomProduit:%s",produit.getId(),produit.getNomProduit()));
            }
            return produits.toArray(new String[0]);
        }
        else
            return new String[]{Objects.requireNonNull(response.getHeaders().get("Custom-Error")).toString()};

    }

}
