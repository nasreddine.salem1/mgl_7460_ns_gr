package com.uqam.employeeBanque;

import com.uqam.employeeBanque.reste.api.employee.EmployeBanqueApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Objects;


/*Rejette le produit demandé par le client*/

@Component("reject")
public class CommandEmployeeReject extends CommandEmployeeGestionProduit implements CommandEmployee{

    private static String linecmdreject ="^--reject(?<produitid>\\s\\d*)\\s*--client(?<client>(\\s\\w+|\\s\\w+\\s\\w+))$";
    @Autowired
    private EmployeBanqueApi clientEmployeBanqueApi;


    @Override
    public String[] executeCmd(String lineCmd) {

        if(!super.valide(lineCmd, linecmdreject))
        {
            return new String[] {"Erreur rejeter commande."};
        }

        String client = super.getClient().toLowerCase();
        String prouitid= super.getProuitid();
        ResponseEntity<String> response =clientEmployeBanqueApi.reject(prouitid,client);

        if (response.getStatusCode().is2xxSuccessful()) {
            return new String[] {response.getBody()};
        }
        else
            return new String[]{Objects.requireNonNull(response.getHeaders().get("Custom-Error")).toString()};


    }
}
