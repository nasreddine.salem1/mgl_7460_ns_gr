package com.uqam.employeeBanque;


import com.uqam.api.rest.v1.ressource.AllClientRessource;
import com.uqam.api.rest.v1.ressource.ClientRessource;
import com.uqam.employeeBanque.reste.api.employee.EmployeBanqueApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

//Liste les clients avec des produits en attente de validation

@Component("tasks")
public class CommandEmployeeTask implements CommandEmployee{
    private static String patternlinecmdtask ="^--tasks$";
    @Autowired
    private EmployeBanqueApi clientEmployeBanqueApi;

    @Override
    public String[] executeCmd(String lineCmd) {


        if(!lineCmd.matches(patternlinecmdtask))
        {
            return new String[] {"Erreur task commande."};
        }

        ResponseEntity<AllClientRessource> response=  clientEmployeBanqueApi.task();
        if (response.getStatusCode().is2xxSuccessful()) {
            List<String> clients = new ArrayList<String>();
            for (ClientRessource client:response.getBody()) {
                clients.add( String.format("clientId:%s, Name:%s", client.getClientId().toString(),client.getClientName()));
            }
            return clients.toArray(new String[0]);
        }
        else
            return new String[]{Objects.requireNonNull(response.getHeaders().get("Custom-Error")).toString()};

    }
}
