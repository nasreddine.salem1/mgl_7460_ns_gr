package com.uqam.employeeBanque;

import com.uqam.api.rest.v1.ressource.ClientRessource;
import com.uqam.employeeBanque.reste.api.employee.EmployeBanqueApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component("upgrade")
public class CommandEmployeeUpgrade extends CommandEmployeeGestionClient   implements CommandEmployee{

    private static String patternlinecmdupgrade ="^--upgrade(?<client>(\\s\\w+|\\s\\w+\\s\\w+))$";
    @Autowired
    private EmployeBanqueApi clientEmployeBanqueApi;
    @Override
    public String[] executeCmd(String lineCmd) {

        if(!super.valide( lineCmd, patternlinecmdupgrade))
        {
            return new String[] {"Erreur Upgrade commande."};
        }

        ClientRessource client =  super.getClient();
        client.setClientStatut(1);
        ResponseEntity<String> response =  clientEmployeBanqueApi.updateStatus(client);
        if (response.getStatusCode().is2xxSuccessful()) {
            return new String[] {response.getBody()};
        }
        else
            return new String[]{Objects.requireNonNull(response.getHeaders().get("Custom-Error")).toString()};


    }
}
