package com.uqam.employeeBanque;

import com.uqam.employeeBanque.config.ApplicationContextEmployeeProvider;
import com.uqam.employeeBanque.config.ConfigurationEnploye;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@EnableAutoConfiguration
@SpringBootApplication
@Import(ConfigurationEnploye.class)
public class EmployeeBanqueApplication implements CommandLineRunner {
    private static final Logger LOG = LoggerFactory.getLogger(EmployeeBanqueApplication.class);

    public static void main(String[] args) {

        SpringApplication.run( EmployeeBanqueApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        if(args== null ||  args.length==0)
            return;

        String cmdLine =  getcmd(args);
        String beanname = getcode(cmdLine);
        //Obtenir le cmd via factory bean
        LOG.info("*****************Reponse Banque cmd employee***************");
        CommandEmployee cmd = (CommandEmployee) ApplicationContextEmployeeProvider.getApplicationContext().getBean(beanname);
        String[] responses = cmd.executeCmd(cmdLine);

        for (String info:responses) {
            LOG.info(info);
        }
        System.exit(0);
    }

    public String getcmd(String[] args) {
        StringBuilder bld = new StringBuilder();
        for (int i = 0; i < args.length; ++i) {
            bld.append(args[i]);
            bld.append(" ");
        }
        return bld.toString().trim();
    }

    public String getcode( String cmdLine )
    {
        String patterncode = "^--(?<codecmd>(add|list|accept|reject|tasks|upgrade|downgrade|exit))\\s*\\w*.*$";
        Pattern pattern = Pattern.compile(patterncode,Pattern.CASE_INSENSITIVE);
        Matcher matcherCmd= pattern.matcher(cmdLine);
        matcherCmd.find();

        if(matcherCmd.matches())
        {
            return matcherCmd.group("codecmd").replace("--","").toLowerCase();
        }
        else{
            //Aucune cmd trouvee, on sort
            return "exit";
        }

    }
}
