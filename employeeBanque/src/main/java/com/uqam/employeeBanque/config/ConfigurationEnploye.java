package com.uqam.employeeBanque.config;

import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.Ordered;

@Configuration
@PropertySource("classpath:application.properties")
@AutoConfigureOrder(Ordered.HIGHEST_PRECEDENCE)
public class ConfigurationEnploye {

    @Bean
    public ApplicationContextEmployeeProvider autowiredApplicationContextProvider()
    {
        return  new ApplicationContextEmployeeProvider();
    }

}
