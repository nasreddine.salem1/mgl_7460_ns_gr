package com.uqam.employeeBanque.reste.api.employee;

import com.uqam.api.rest.v1.ressource.AllClientRessource;
import com.uqam.api.rest.v1.ressource.AllProduitRessource;
import com.uqam.api.rest.v1.ressource.ClientRessource;
import com.uqam.api.rest.v1.ressource.ProduitRessource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public interface EmployeBanqueApi {

    ResponseEntity<ClientRessource> add(ClientRessource client) ;

    ResponseEntity<AllProduitRessource>  list(String client) ;

    ResponseEntity<String>  accept(ProduitRessource produitRessource, String client) ;

    ResponseEntity<String>  reject(String produitRessource, String client) ;

    ResponseEntity<AllClientRessource> task();

    ResponseEntity<String> updateStatus(ClientRessource client);
    String getUrl() ;

    String getUrl(String operation);

    String getUrl(String operation,String clientname) ;




}
