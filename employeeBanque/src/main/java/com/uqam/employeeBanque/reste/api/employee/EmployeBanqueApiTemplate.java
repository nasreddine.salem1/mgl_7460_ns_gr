package com.uqam.employeeBanque.reste.api.employee;

import com.uqam.api.rest.v1.ressource.AllClientRessource;
import com.uqam.api.rest.v1.ressource.AllProduitRessource;
import com.uqam.api.rest.v1.ressource.ClientRessource;
import com.uqam.api.rest.v1.ressource.ProduitRessource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
@PropertySource("classpath:application.properties")
public class EmployeBanqueApiTemplate implements EmployeBanqueApi{
    public static final String SUCCESSFUL_OPERATION = "successful operation";
    public static final String LE_CLIENT_N_EST_PAS_VALIDE = "le client n'est pas valide";
    public static final String LE_PRODUIT_N_EST_PAS_VALIDE = "le produit n'est pas valide";
    //TODO: mettre dans des variable

    private String service="employee";
    private String url="http://localhost:8787/api/v1";


    public String getUrl(){
        return String.format("%s/%s",  url , service);
    }

    public String getUrl(String operation)  {
        return  String.format("%s/%s/%s",  url , service, operation);
    }

    public String getUrl(String operation,String clientname)  {
       return  String.format("%s/%s/%s/%s",  url , service, operation,clientname);
    }
    public String getUrl(String operation,String clientname,long produitid)  {
        return String.format("%s/%s/%s/%s/%d",  url , service, operation,clientname,produitid);
    }
    @Override
    public ResponseEntity<ClientRessource> add(ClientRessource client) {

        if(client==null)
            throw new NullPointerException(LE_CLIENT_N_EST_PAS_VALIDE);

        String urlBanqueApi = getUrl();
        try
        {
            RestTemplate restTemplate = new RestTemplate();
            return restTemplate.postForEntity(urlBanqueApi, client, ClientRessource.class);
        }
        catch (HttpClientErrorException ex)
        {

            return this.<ClientRessource>getResponseEntity(ex);
        }

    }

    @Override
    public ResponseEntity<AllProduitRessource> list(String nomclient) {
        if (nomclient == null)
            throw new NullPointerException(LE_CLIENT_N_EST_PAS_VALIDE);

        try
        {
            String urlApi = getUrl(nomclient);
            RestTemplate restTemplate = new RestTemplate();
            return restTemplate.getForEntity( urlApi, AllProduitRessource.class);

        }
        catch (HttpClientErrorException ex)
        {
            return this.<AllProduitRessource>getResponseEntity(ex);
        }
    }

    private <T> ResponseEntity getResponseEntity(HttpClientErrorException ex) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Custom-Error", ex.getMessage());
        return new ResponseEntity<T>(headers, ex.getStatusCode());
    }

    @Override
    public ResponseEntity<String>  accept(ProduitRessource produitRessource, String nomclient) {

        if (produitRessource == null)
            throw new NullPointerException(LE_PRODUIT_N_EST_PAS_VALIDE);

        if (nomclient == null)
            throw new NullPointerException(LE_CLIENT_N_EST_PAS_VALIDE);

        try
        {
            String urlApi = getUrl("accept",nomclient.toLowerCase(),produitRessource.getId());
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.put(urlApi,produitRessource);
            return  new ResponseEntity<String>(SUCCESSFUL_OPERATION, HttpStatus.OK  );
        }
        catch (HttpClientErrorException ex)
        {
            return this.<String>getResponseEntity(ex);
        }

    }
    @Override
    public ResponseEntity<String> reject(String produitid, String nomclient) {

        if (produitid == null)
            throw new NullPointerException(LE_PRODUIT_N_EST_PAS_VALIDE);

        if (nomclient == null)
            throw new NullPointerException(LE_CLIENT_N_EST_PAS_VALIDE);


        try
        {
            String urlApi = getUrl("reject",nomclient, Long.parseLong(produitid));
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.delete(urlApi);
            return new ResponseEntity<String>( SUCCESSFUL_OPERATION , HttpStatus.OK  );
        }
        catch (HttpClientErrorException ex)
        {
            return this.<String>getResponseEntity(ex);
        }

    }
    @Override
    public ResponseEntity<AllClientRessource> task()
    {
        try
        {
            String urlApi = getUrl("task");
            RestTemplate restTemplate = new RestTemplate();
            return restTemplate.getForEntity(urlApi, AllClientRessource.class);

        }
        catch (HttpClientErrorException ex)
        {
            return this.<String>getResponseEntity(ex);
        }
    }

    @Override
    public ResponseEntity<String> updateStatus(ClientRessource client) {

        if(client==null)
            throw new NullPointerException(LE_CLIENT_N_EST_PAS_VALIDE);

        String urlBanqueApi = getUrl("updateStatus");
        try
        {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.put(urlBanqueApi, client);
            return new ResponseEntity<String>( SUCCESSFUL_OPERATION , HttpStatus.OK  );
        }
        catch (HttpClientErrorException ex)
        {

            return this.<String>getResponseEntity(ex);
        }
}
}

