package com.uqam.employeeBanque.reste.api.employee;

public class ExceptionApi extends Exception {

    public ExceptionApi(String message)
    {
        super(message);
    }
}