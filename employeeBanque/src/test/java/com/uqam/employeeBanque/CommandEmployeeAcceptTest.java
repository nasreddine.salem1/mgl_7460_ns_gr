package com.uqam.employeeBanque;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.uqam.employeeBanque.config.ConfigurationEnploye;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EmployeeBanqueApplication.class)
@ContextConfiguration(classes = ConfigurationEnploye.class)
public class CommandEmployeeAcceptTest {

    private  WireMockServer wm = new WireMockServer(options().port(8787));

    @Autowired
    private CommandEmployeeAccept cmdAccept;
    @Before
    public  void Init()
    {
        //-- accept
        wm.stubFor(put(urlEqualTo("/api/v1/employee/accept/graccept/21245"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-Type", TEXT_PLAIN_VALUE)
                        .withBody("successful operation")
                )
        );



        wm.start();
    }

    @After
    public  void After()
    {
        wm.stop();
    }


    @Test
    public void executeErrorTest() throws InterruptedException {
        String cmd = "--accept 21247 --client GRERROR";
        Thread.sleep(3000);
        String[] response =cmdAccept.executeCmd(cmd);

        Assert.assertEquals("[404 Not Found]",response[0]);

    }
    @Test
    public void executeWithOutClientParamTest() throws InterruptedException {
        String cmd = "--accept 21245 --client";
        Thread.sleep(3000);
        String[] response =cmdAccept.executeCmd(cmd);

        Assert.assertEquals("Erreur commande.",response[0]);

    }

    @Test
    public void executeWrongParamNumberTest()
    {
        String cmd = "--accept 21adadasd245 --client GRAccept";

        String[] response =cmdAccept.executeCmd(cmd);

        Assert.assertEquals("Erreur commande.",response[0]);

    }
}
