package com.uqam.employeeBanque;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.uqam.api.rest.v1.ressource.ClientRessource;
import com.uqam.employeeBanque.config.ConfigurationEnploye;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EmployeeBanqueApplication.class)
@ContextConfiguration(classes = ConfigurationEnploye.class)
public class CommandEmployeeAddTest {

    private  WireMockServer wm = new WireMockServer(options().port(8787));
    @Autowired
    private CommandEmployeeAdd cmdAdd;

    @Before
    public  void Init()
    {
        //-- add
        wm.stubFor(post(urlEqualTo("/api/v1/employee"))
                .withRequestBody( equalToJson("{ \"clientId\":null,\"clientName\":\"gr\",\"email\":\"\",\"phone\":\"\", \"clientStatut\": 0}"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-Type", APPLICATION_JSON_VALUE)
                        .withBody("{\"clientId\": 10,\"clientName\": \"Gorgio Armani\",\"email\": \"g@dgmail.com\",\"phone\": \"514-999-9999\", \"clientStatut\": 0}")
                )
        );
        //-- add
        wm.stubFor(post(urlEqualTo("/api/v1/employee"))
                .withRequestBody( equalToJson("{ \"clientId\":0,\"clientName\":\"grerror\",\"email\":\"\",\"phone\":\"\", \"clientStatut\": 1}"))
                .willReturn(aResponse()
                        .withStatus(400)
                        .withHeader("Content-Type", TEXT_PLAIN_VALUE)
                        .withBody("400 Bad Request")

                )
        );
        wm.start();
    }

    @After
    public  void After()
    {
        wm.stop();
    }

    @Test
    public void executeCmdAddTest()
    {
        String[] resultat= cmdAdd.executeCmd("--add GR");

        Assert.assertEquals(1,resultat.length);
        Assert.assertNotEquals("Erreur commande.",resultat[0]);
        Assert.assertNotEquals("[400 Bad Request]",resultat[0]);
    }

    @Test
    public void executeCmdAddErreurTest()
    {
        String[] resultat= cmdAdd.executeCmd("--addGorgio");

        Assert.assertEquals(1,resultat.length);
        Assert.assertEquals("Erreur commande.",resultat[0]);
    }

    @Test
    public void executeCmdAddErreur404URLTest()
    {
        String[] resultat= cmdAdd.executeCmd("--add GRERROR");

        Assert.assertEquals(1,resultat.length);
        Assert.assertEquals("[400 Bad Request]",resultat[0]);
    }

    @Test
    public void getClientTest()
    {
        ClientRessource client =  cmdAdd.getClient("Batman");
        Assert.assertNotNull(client);
        Assert.assertEquals("batman", client.getClientName());
    }
}
