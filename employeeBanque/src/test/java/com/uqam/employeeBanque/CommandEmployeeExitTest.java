package com.uqam.employeeBanque;

import com.uqam.employeeBanque.config.ConfigurationEnploye;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EmployeeBanqueApplication.class)
@ContextConfiguration(classes = ConfigurationEnploye.class)

public class CommandEmployeeExitTest {

    @Autowired
    private CommandEmployeeExit cmdExit;

    @Test
    public void cmdexitTest()
    {
        String[] response = cmdExit.executeCmd(null);

        Assert.assertEquals("Bye",response[0]);
    }
}
