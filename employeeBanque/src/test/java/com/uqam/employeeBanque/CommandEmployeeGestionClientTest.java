package com.uqam.employeeBanque;

import com.uqam.employeeBanque.config.ConfigurationEnploye;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EmployeeBanqueApplication.class)
@ContextConfiguration(classes = ConfigurationEnploye.class)
public class CommandEmployeeGestionClientTest {

    @Test
    public void valideRejectTest()
    {
        String patternlinecmdupgrade ="^--upgrade(?<client>(\\s\\w+|\\s\\w+\\s\\w+))$";
        CommandEmployeeGestionClient cmd = new CommandEmployeeGestionClient();

        Assert.assertTrue(cmd.valide("--upgrade Batman", patternlinecmdupgrade));
        Assert.assertNotNull(cmd.getClient());
    }

    @Test
    public void notValideRejectTest()
    {
        String patternlinecmdupgrade ="^--upgrssade(?<client>(\\s\\w+|\\s\\w+\\s\\w+))$";
        CommandEmployeeGestionClient cmd = new CommandEmployeeGestionClient();
        Assert.assertFalse(cmd.valide("--upgsassrade Batman", patternlinecmdupgrade));
    }
}


