package com.uqam.employeeBanque;

import com.uqam.employeeBanque.config.ConfigurationEnploye;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EmployeeBanqueApplication.class)
@ContextConfiguration(classes = ConfigurationEnploye.class)
public class CommandEmployeeGestionTest {

    @Test
    public void valideRejectTest()
    {
        String linecmdreject ="^--reject(?<produitid>\\s\\d*)\\s*--client(?<client>(\\s\\w+|\\s\\w+\\s\\w+))$";

        CommandEmployeeGestionProduit cmd = new CommandEmployeeGestionProduit();

        Assert.assertTrue(cmd.valide("--reject 10 --client GR", linecmdreject));
        Assert.assertEquals("GR",cmd.getClient());
        Assert.assertEquals("10",cmd.getProuitid());
    }

    @Test
    public void notValideRejectTest()
    {
        String linecmdreject ="^--rejhhhect(?<produitid>\\s\\d*)\\s*--client(?<client>(\\s\\w+|\\s\\w+\\s\\w+))$";

        CommandEmployeeGestionProduit cmd = new CommandEmployeeGestionProduit();

        Assert.assertFalse(cmd.valide("--reject 10 --client GR", linecmdreject));

    }
}
