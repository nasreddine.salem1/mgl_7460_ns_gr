package com.uqam.employeeBanque;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.uqam.employeeBanque.config.ConfigurationEnploye;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EmployeeBanqueApplication.class)
@ContextConfiguration(classes = ConfigurationEnploye.class)
public class CommandEmployeeListTest {

    private  WireMockServer wm = new WireMockServer(options().port(8787));

    @Autowired
    private CommandEmployeeList cmdList;

    @Before
    public  void Init()
    {
        //-- list
        wm.stubFor(get(urlEqualTo("/api/v1/employee/GR"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-Type", APPLICATION_JSON_VALUE)
                        .withBody("[{\"id\":10,\"clientId\":20,\"nomProduit\":\"tests1\",\"statut\":\"accept\"},{\"id\":30,\"clientId\":20,\"nomProduit\":\"tests2\",\"statut\":\"reject\"}]")
                )
        );
        wm.stubFor(get(urlEqualTo("/api/v1/employee/Alain"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.NOT_FOUND.value())

                )
        );
        wm.start();
    }

    @After
    public  void After()
    {
        wm.stop();
    }

    @Test
    public void executeCmdListTest()
    {
        String[] resultat= cmdList.executeCmd("--list GR");

        Assert.assertEquals(2,resultat.length);
        Assert.assertNotEquals("Erreur commande.",resultat[0]);
        Assert.assertEquals( "id:(10) NomProduit:tests1",resultat[0]);
        Assert.assertEquals("id:(30) NomProduit:tests2",resultat[1]);

    }
    @Test
    public void executeCmdErreurTypeListTest()
    {
        String[] resultat= cmdList.executeCmd("--lissssst GR");

        Assert.assertEquals(1,resultat.length);
        Assert.assertEquals("Erreur commande.",resultat[0]);

    }

    @Test
    public void executeCmdListClient404Test()
    {
        try {
            String[] resultat= cmdList.executeCmd("--list Alain");
        }
       catch (HttpClientErrorException ex)
       {

       Assert.assertEquals(404,ex.getStatusCode().value());
       }



    }
}
