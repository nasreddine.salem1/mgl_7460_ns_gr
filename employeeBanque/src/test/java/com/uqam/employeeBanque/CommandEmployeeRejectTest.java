package com.uqam.employeeBanque;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.uqam.employeeBanque.config.ConfigurationEnploye;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EmployeeBanqueApplication.class)
@ContextConfiguration(classes = ConfigurationEnploye.class)
public class CommandEmployeeRejectTest {
    private  WireMockServer wm = new WireMockServer(options().port(8787));

    @Autowired
    private CommandEmployeeReject cmdReject;

    @Before
    public  void Init()
    {
        //-- reject
        wm.stubFor(delete(urlEqualTo("/api/v1/employee/reject/gr/20"))
        .willReturn(aResponse()
                .withStatus(HttpStatus.NOT_FOUND.value())));

        //-- reject
        wm.stubFor(delete(urlEqualTo("/api/v1/employee/reject/gr/10")));
        wm.start();
    }

    @After
    public  void After()
    {
        wm.stop();
    }


    @Test
    public void executeTest()
    {
        String cmd = "--reject 10 --client GR";

        String[] response =cmdReject.executeCmd(cmd);

        Assert.assertEquals("successful operation",response[0]);

    }

    @Test
    public void executeErrorTest()
    {
        String cmd = "--reject 20 --client GR";

        String[] response =cmdReject.executeCmd(cmd);

        Assert.assertEquals("[404 Not Found]",response[0]);

    }
    @Test
    public void executeWithOutClientParamTest()
    {
        String cmd = "--reject 10 --client";

        String[] response =cmdReject.executeCmd(cmd);

        Assert.assertEquals("Erreur rejeter commande.",response[0]);

    }

    @Test
    public void executeWrongParamNumberTest()
    {
        String cmd = "--reject 21adadasd245 --client GRAccept";

        String[] response =cmdReject.executeCmd(cmd);

        Assert.assertEquals("Erreur rejeter commande.",response[0]);

    }
}
