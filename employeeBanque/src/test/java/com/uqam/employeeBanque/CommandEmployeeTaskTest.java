package com.uqam.employeeBanque;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.uqam.employeeBanque.config.ConfigurationEnploye;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EmployeeBanqueApplication.class)
@ContextConfiguration(classes = ConfigurationEnploye.class)
public class CommandEmployeeTaskTest {

    private  WireMockServer wm = new WireMockServer(options().port(8787));

    @Autowired
    private CommandEmployeeTask cmdtask;



    @Before
    public void Init()
    {
        //-- list
        wm.stubFor(get(urlEqualTo("/api/v1/employee/task"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-Type", APPLICATION_JSON_VALUE)
                        .withBody("[{ \"clientId\":9, \"clientName\":\"Martin\", \"email\":\"\", \"phone\":\"\", \"clientStatut\":0}]")
                )
        );

        wm.start();
    }

    @After
    public  void After()
    {
        wm.stop();
    }


    @Test
    public void taskTest()
    {
        String[] ressource = cmdtask.executeCmd("--tasks");
        Assert.assertEquals(1,ressource.length);
       Assert.assertEquals("clientId:9, Name:Martin",ressource[0]);
    }

    @Test
    public void taskErrorTest()
    {
        String[] ressource = cmdtask.executeCmd("--tappsk");
        Assert.assertEquals("Erreur task commande.",ressource[0]);

    }
}
