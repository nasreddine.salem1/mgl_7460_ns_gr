package com.uqam.employeeBanque;

import com.uqam.employeeBanque.config.ConfigurationEnploye;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EmployeeBanqueApplication.class)
@ContextConfiguration(classes = ConfigurationEnploye.class)
public class CommandEmployeeUpgradeTest {

    @Autowired
    private CommandEmployeeUpgrade cmd;

    @Autowired
    private CommandEmployeeDownGrade cmdDownGrade;


    @Test
    public void executeCmdUpgradeErrorTest()
    {
        String[] resultat= cmd.executeCmd("--upgrade ");
        Assert.assertEquals(1,resultat.length);
        Assert.assertEquals("Erreur Upgrade commande.",resultat[0]);
    }


    @Test
    public void executeCmdDownGradeErrorTest()
    {
        String[] resultat= cmdDownGrade.executeCmd("--downgrade ");
        Assert.assertEquals(1,resultat.length);
        Assert.assertEquals("Erreur Downgrade commande.",resultat[0]);
    }

}
