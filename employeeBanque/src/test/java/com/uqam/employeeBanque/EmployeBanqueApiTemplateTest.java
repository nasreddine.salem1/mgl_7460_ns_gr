package com.uqam.employeeBanque;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.uqam.api.rest.v1.ressource.AllClientRessource;
import com.uqam.api.rest.v1.ressource.AllProduitRessource;
import com.uqam.api.rest.v1.ressource.ClientRessource;
import com.uqam.api.rest.v1.ressource.ProduitRessource;
import com.uqam.employeeBanque.config.ConfigurationEnploye;
import com.uqam.employeeBanque.reste.api.employee.EmployeBanqueApi;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EmployeeBanqueApplication.class)
@ContextConfiguration(classes = ConfigurationEnploye.class)
public class EmployeBanqueApiTemplateTest {

    private  WireMockServer wm = new WireMockServer(options().port(8787));

    @Autowired
    private EmployeBanqueApi api;

    @Before
    public  void Init()
    {
        //-- add
        wm.stubFor(post(urlEqualTo("/api/v1/employee"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-Type", APPLICATION_JSON_VALUE)
                        .withBody("{\"clientId\": 10,\"clientName\": \"Gorgio Armani\",\"email\": \"g@dgmail.com\",\"phone\": \"514-999-9999\", \"clientStatut\": 0}")
                        )
                );
        //-- list
        wm.stubFor(get(urlEqualTo("/api/v1/employee/GR"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-Type", APPLICATION_JSON_VALUE)
                        .withBody("[{\"id\":10,\"clientId\":20,\"nomProduit\":\"tests1\",\"statut\":\"accept\"},{\"id\":30,\"clientId\":20,\"nomProduit\":\"tests2\",\"statut\":\"reject\"}]")
                )
        );
        //-- accept
        wm.stubFor(put(urlEqualTo("/api/v1/employee/accept/gr/5"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-Type", TEXT_PLAIN_VALUE )
                        .withBody("successful operation")
                )
        );
        //-- reject
        wm.stubFor(delete(urlEqualTo("/api/v1/employee/reject/GR/10")));

        //-- task
        wm.stubFor(get(urlEqualTo("/api/v1/employee/task"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-Type", APPLICATION_JSON_VALUE)
                        .withBody("[{ \"clientId\":9, \"clientName\":\"Martin\", \"email\":\"\", \"phone\":\"\", \"clientStatut\":0}]")
                )
        );

        //-- updateStatus
        wm.stubFor(put(urlEqualTo("/api/v1/employee/updateStatus"))
                .withRequestBody( equalToJson("{ \"clientId\":null,\"clientName\":\"batman\",\"email\":null,\"phone\":null, \"clientStatut\":null}"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-Type", TEXT_PLAIN_VALUE )
                        .withBody("successful operation")
                )
        );
        wm.start();
    }

    @After
    public  void After()
    {
        wm.stop();
    }

    @Test
    public void addClientTest()
    {
        ClientRessource client = new ClientRessource();
        client.clientName("Gorgio Armani");
        ResponseEntity<ClientRessource> ressource = api.add(client);

        Long expected= Long.valueOf(10);
        Assert.assertEquals(expected,  ressource.getBody().getClientId());
    }

    @Test
    public void addClientNullTest()
    {
        try{
            ResponseEntity<ClientRessource> resultat= api.add(null);
            Assert.fail("NullPointerException ne s'est pas active");
        }
        catch (NullPointerException ex)
        {

        }
    }
    @Test
    public void lisClientTest()
    {

        ResponseEntity<AllProduitRessource> resultat= api.list("GR");
        AllProduitRessource produits = resultat.getBody();

        Assert.assertEquals(2,resultat.getBody().size());

    }
    @Test
    public void lisClientNullTest()
    {
        try{
            ResponseEntity<AllProduitRessource> resultat= api.list(null);
            Assert.fail("NullPointerException ne s'est pas active");
        }
        catch (NullPointerException ex)
        {

        }

    }
    @Test
    public void addClientaddNullTest()
    {
        try{
            ResponseEntity<ClientRessource> ressource = api.add(null);
            Assert.fail("Erreur de validation client");
        }catch (NullPointerException ex)
        { }
        catch (Exception ex)
        {
            Assert.fail("Mauvaises exception");
        }

    }

    @Test
    public void getUrlTest()
    {
        String url = api.getUrl();
        Assert.assertEquals("http://localhost:8787/api/v1/employee",url);
    }

    @Test
    public void getUrlOperationTest()
    {
        String url = api.getUrl("add");
        Assert.assertEquals("http://localhost:8787/api/v1/employee/add",url);
    }
    @Test
    public void getUrlOperationNameTest()
    {
        String url = api.getUrl("add","GR");
        Assert.assertEquals("http://localhost:8787/api/v1/employee/add/GR",url);
    }
    @Test
    public void getUrlOperationNameProduitTest()
    {
        String url = api.getUrl("produit","GR");
        Assert.assertEquals("http://localhost:8787/api/v1/employee/produit/GR",url);
    }



    @Test
    public void acceptTest() throws InterruptedException {
        Thread.sleep(5000);
        ProduitRessource produitRessource  = new ProduitRessource();
        produitRessource.setId(Long.parseLong("5"));
        ResponseEntity<String> ressource = api.accept(produitRessource,"GR");
        String expected= "successful operation";
        Assert.assertEquals(expected,  ressource.getBody());
    }
    @Test
    public void acceptNullProduitTest()
    {


        try{
            ResponseEntity<String> ressource = api.accept(null,"GR");
            Assert.fail("Erreur de validation produit");
        }catch (NullPointerException ex)
        { }
        catch (Exception ex)
        {
            Assert.fail("Mauvaises exception");
        }

    }

    @Test
    public void acceptNullClientTest()
    {
        try{
            ProduitRessource produitRessource  = new ProduitRessource();
            produitRessource.setId(Long.getLong("1254"));
            ResponseEntity<String> ressource = api.accept(produitRessource,null);
            Assert.fail("Erreur de validation client");
        }catch (NullPointerException ex)
        { }
        catch (Exception ex)
        {
            Assert.fail("Mauvaises exception");
        }

    }


    @Test
    public void rejectTest() throws InterruptedException {
        Thread.sleep(5000);
        ResponseEntity<String> ressource = api.reject("10","GR");
        String expected= "successful operation";
        Assert.assertEquals(expected,  ressource.getBody());
    }
    @Test
    public void rejectNullProduitTest()
    {


        try{
            ResponseEntity<String> ressource = api.reject(null,"GR");
            Assert.fail("Erreur de validation produit");
        }catch (NullPointerException ex)
        { }
        catch (Exception ex)
        {
            Assert.fail("Mauvaises exception");
        }

    }

    @Test
    public void rejectNullClientTest()
    {
        try{

            ResponseEntity<String> ressource = api.reject("10",null);
            Assert.fail("Erreur de validation client");
        }catch (NullPointerException ex)
        { }
        catch (Exception ex)
        {
            Assert.fail("Mauvaises exception");
        }

    }

    @Test
    public void taskTest()
    {
        ResponseEntity<AllClientRessource> ressource = api.task();
        Assert.assertEquals(1,ressource.getBody().size());
        Assert.assertEquals("Martin",ressource.getBody().get(0).getClientName());
    }

    @Test
    public void updateStatusTest() throws InterruptedException {
        Thread.sleep(5000);
        ClientRessource client = new ClientRessource();
        client.clientName("batman");
        ResponseEntity<String> ressource = api.updateStatus(client);
        String expected= "successful operation";
        Assert.assertEquals(expected,  ressource.getBody());

    }

    @Test
    public void updateStatusClientaddNullTest()
    {
        try{
            ResponseEntity<ClientRessource> ressource = api.add(null);
            Assert.fail("Erreur de validation client");
        }catch (NullPointerException ex)
        { }
        catch (Exception ex)
        {
            Assert.fail("Mauvaises exception");
        }

    }


}


