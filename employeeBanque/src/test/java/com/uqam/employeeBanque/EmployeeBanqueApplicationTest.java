package com.uqam.employeeBanque;

import com.uqam.employeeBanque.config.ConfigurationEnploye;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EmployeeBanqueApplication.class)
@ContextConfiguration(classes = ConfigurationEnploye.class)
public class EmployeeBanqueApplicationTest {


    @Test
    public void runArgsZeroTest() {
        CommandLineRunner app = new EmployeeBanqueApplication();
        try {
            app.run(new String[0]);

        } catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void runArgsEmptyTest() {
        CommandLineRunner app = new EmployeeBanqueApplication();
        try {
            app.run(new String[]{});

        } catch (Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void getCodeAdd()
    {
        EmployeeBanqueApplication app = new EmployeeBanqueApplication();
        String code= app.getcode("--add CLIENT_NAME");
        Assert.assertEquals("add", code);
    }
    @Test
    public void getCodeList()
    {
        EmployeeBanqueApplication app = new EmployeeBanqueApplication();
        String code= app.getcode("--list CLIENT_NAME");
        Assert.assertEquals("list", code);
    }

    @Test
    public void getCodeExit()
    {
        EmployeeBanqueApplication app = new EmployeeBanqueApplication();
        String code= app.getcode("--exit");
        Assert.assertEquals("exit", code);
    }

    @Test
    public void getCodeExitVide()
    {
        EmployeeBanqueApplication app = new EmployeeBanqueApplication();
        String code= app.getcode("");
        Assert.assertEquals("exit", code);
    }

    @Test
    public void getCmd()
    {
        EmployeeBanqueApplication app = new EmployeeBanqueApplication();
        String[] args = new String[] {"--add","GR"};
        String line = app.getcmd(args);
        Assert.assertEquals("--add GR", line);
    }

}
