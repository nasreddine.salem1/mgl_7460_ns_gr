package com.uqam.employeeBanque;

import com.uqam.employeeBanque.config.ConfigurationEnploye;
import com.uqam.employeeBanque.reste.api.employee.ExceptionApi;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EmployeeBanqueApplication.class)
@ContextConfiguration(classes = ConfigurationEnploye.class)
public class ExceptionApiTest {

    @Test
    public void raiseTest()
    {
        try {
            throw new ExceptionApi("TU exception");
        }
        catch (ExceptionApi ex)
        {
            Assert.assertEquals("TU exception",ex.getMessage());
        }

    }
}
