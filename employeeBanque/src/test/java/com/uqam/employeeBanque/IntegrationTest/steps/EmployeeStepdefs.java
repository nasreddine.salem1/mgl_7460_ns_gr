package com.uqam.employeeBanque.IntegrationTest.steps;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.uqam.employeeBanque.CommandEmployeeAdd;
import com.uqam.employeeBanque.IntegrationTest.CucumberSpringContextConfiguration;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

public class EmployeeStepdefs extends CucumberSpringContextConfiguration {

    private WireMockServer wm = new WireMockServer(options().port(8787));
    private String commande_entree= "";
    private String[] clients;

    @Autowired
    private CommandEmployeeAdd cmdAdd;

    @Before
    public void Init()
    {

        //-- add
        wm.stubFor(
                post(urlEqualTo("/api/v1/employee"))

                        .willReturn(aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-Type", APPLICATION_JSON_VALUE)
                        .withBody("{\"clientId\": 10,\"clientName\": \"Gorgio Armani\",\"email\": \"g@dgmail.com\",\"phone\": \"514-999-9999\", \"clientStatut\": 0}")
                )
        );
        wm.start();

    }
    @After
    public void Clean()
    {
        wm.stop();
    }

    @Given("l'employee saisi la commande suivante: {string}")
    public void l_employee_saisi_la_commande_suivante(String cmd) {
        commande_entree=cmd.trim();
    }

    @When("le client appuie sur enter:")
    public void le_client_appuie_sur_enter() {

        clients= cmdAdd.executeCmd(commande_entree);

    }

    @Then("la requete affiche resultat suivant:")
    public void la_requete_affiche_resultat_suivant() {
        for (String client:clients)
            System.out.println(client);
    }

    @And("le client est ajoute:  {int}")
    public void le_client_est_ajoute(int arg0) {
        Assert.assertEquals(1,clients.length);


        if(commande_entree.equals("--add Gordano Totti")){
            Assert.assertNotEquals("Erreur commande.",clients[0]);
            Assert.assertEquals("10 Gorgio Armani g@dgmail.com 0",clients[0]);
        }
        else
        {
            Assert.assertEquals("Erreur commande.",clients[0]);

        }
    }
}
